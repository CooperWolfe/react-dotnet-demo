using MediatR;
using Backend.Core.Transactions;
using Backend.Core.Commands.Responses;
using Backend.Core.Commands;
using Backend.Core.Validation.Mediator;
using Microsoft.Extensions.Logging;

namespace Backend.Core;
internal class BackendService : IBackendService
{
    private readonly IUnitOfWorkFactory<IBackendUnitOfWork> backendUnitOfWorkFactory;
    private readonly IUnitOfWorkFactory<IImageUnitOfWork> imageUnitOfWorkFactory;
    private readonly IMediator mediator;
    private readonly ICommandValidator validator;
    private readonly ILogger<BackendService> logger;

    public BackendService(
        IUnitOfWorkFactory<IBackendUnitOfWork> backendUnitOfWorkFactory,
        IUnitOfWorkFactory<IImageUnitOfWork> imageUnitOfWorkFactory,
        IMediator mediator,
        ICommandValidator validator,
        ILogger<BackendService> logger)
    {
        this.backendUnitOfWorkFactory = backendUnitOfWorkFactory;
        this.imageUnitOfWorkFactory = imageUnitOfWorkFactory;
        this.mediator = mediator;
        this.validator = validator;
        this.logger = logger;
    }

    public Task<CreateRecipeResponse> CreateRecipe(CreateRecipeCommand command, CancellationToken cancellationToken) => ValidateAndExecuteInTransaction(command, cancellationToken);
    public Task<GetOneRecipeResponse> GetOneRecipe(GetOneRecipeCommand command, CancellationToken cancellationToken) => ValidateAndExecute(command, cancellationToken);
    public Task SetRecipeImage(SetRecipeImageCommand command, CancellationToken cancellationToken) => ValidateAndExecuteInImageTransaction(command, cancellationToken);
    public Task<GetRecipeImageResponse> GetRecipeImage(GetRecipeImageCommand command, CancellationToken cancellationToken) => ValidateAndExecute(command, cancellationToken);
    public Task<SearchRecipesResponse> SearchRecipes(SearchRecipesCommand command, CancellationToken cancellationToken) => ValidateAndExecute(command, cancellationToken);
    public Task UpdateRecipe(UpdateRecipeCommand command, CancellationToken cancellationToken) => ValidateAndExecuteInTransaction(command, cancellationToken);
    public Task DeleteRecipeImage(DeleteRecipeImageCommand command, CancellationToken cancellationToken) => ValidateAndExecuteInImageTransaction(command, cancellationToken);
    public async Task DeleteRecipe(DeleteRecipeCommand command, CancellationToken cancellationToken)
    {
        validator.ValidateAndThrow(command);
        using (var imageUnitOfWork = imageUnitOfWorkFactory.Create())
        {
            using (var backendUnitOfWork = backendUnitOfWorkFactory.Create())
            {
                await mediator.Send(command, cancellationToken);
                await backendUnitOfWork.CommitAsync(cancellationToken);
            }

            try
            {
                await imageUnitOfWork.CommitAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"An image-related error occurred after successful deletion of recipe '{command.Id}'. Ignoring.");
            }
        }
    }

    private Task<T> ValidateAndExecute<T>(IRequest<T> command, CancellationToken cancellationToken)
    {
        validator.ValidateAndThrow(command);
        return mediator.Send(command, cancellationToken);
    }
    private async Task<T> ValidateAndExecuteInTransaction<T>(IRequest<T> command, CancellationToken cancellationToken)
    {
        validator.ValidateAndThrow(command);
        T response;
        using (var unitOfWork = backendUnitOfWorkFactory.Create())
        {
            response = await mediator.Send(command, cancellationToken);
            await unitOfWork.CommitAsync(cancellationToken);
        }
        return response;
    }
    private async Task<T> ValidateAndExecuteInImageTransaction<T>(IRequest<T> command, CancellationToken cancellationToken)
    {
        validator.ValidateAndThrow(command);
        T response;
        using (var unitOfWork = imageUnitOfWorkFactory.Create())
        {
            response = await mediator.Send(command, cancellationToken);
            await unitOfWork.CommitAsync(cancellationToken);
        }
        return response;
    }
}