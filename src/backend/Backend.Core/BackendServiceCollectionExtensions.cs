﻿using MediatR;
using Backend.Core;
using Backend.Core.Transactions;
using Backend.Core.Validation.Mediator;
using FluentValidation;
using Backend.Core.Commands;
using Backend.Core.Validation;
using Backend.Core.Utilities;
using Backend.Core.Configuration;
using Microsoft.Extensions.Options;

namespace Microsoft.Extensions.DependencyInjection;
public static class BackendServiceCollectionExtensions
{
    public static void AddCore(this IServiceCollection services, Action<OptionsBuilder<BackendConfiguration>> configure)
    {
        services.AddMediatR(typeof(BackendServiceCollectionExtensions));
        services.AddLogging();
        services.AddLocalization();
        services.AddUnitOfWork<IBackendUnitOfWork, BackendUnitOfWorkFactory>();
        services.AddUnitOfWork<IImageUnitOfWork, ImageUnitOfWorkFactory>();

        var configBuilder = services.AddOptions<BackendConfiguration>();
        configure(configBuilder);

        services.AddTransient<ICommandValidator, MediatorCommandValidator>();

        services.AddScoped<IRequestContext, RequestContext>();

        services.AddTransient<IBackendService, BackendService>();
        services.AddTransient<IValidator<CreateRecipeCommand>, CreateRecipeCommandValidator>();
        services.AddTransient<IValidator<UpdateRecipeCommand>, UpdateRecipeCommandValidator>();
        services.AddTransient<IValidator<GetOneRecipeCommand>, GetOneRecipeCommandValidator>();
        services.AddTransient<IValidator<SearchRecipesCommand>, SearchRecipesCommandValidator>();
        services.AddTransient<IValidator<DeleteRecipeCommand>, DeleteRecipeCommandValidator>();
        services.AddTransient<IValidator<GetRecipeImageCommand>, GetRecipeImageCommandValidator>();
        services.AddTransient<IValidator<SetRecipeImageCommand>, SetRecipeImageCommandValidator>();
        services.AddTransient<IValidator<DeleteRecipeImageCommand>, DeleteRecipeImageCommandValidator>();
    }

    private static void AddUnitOfWork<TUnitOfWork, TUnitOfWorkFactory>(this IServiceCollection services)
        where TUnitOfWork : class, IUnitOfWork
        where TUnitOfWorkFactory : class, IUnitOfWorkFactory<TUnitOfWork>
    {
        services.AddScoped<IUnitOfWorkFactory<TUnitOfWork>, TUnitOfWorkFactory>();
        services.AddTransient<TUnitOfWork>(sp =>
        {
            var unitOfWorkFactory = sp.GetRequiredService<IUnitOfWorkFactory<TUnitOfWork>>();
            var unitOfWork = unitOfWorkFactory.Current;
            if (unitOfWork is null)
            {
                throw new Exception("Unit of work requested by a service outside the scope of a unit of work.");
            }
            return unitOfWork;
        });
    }
}