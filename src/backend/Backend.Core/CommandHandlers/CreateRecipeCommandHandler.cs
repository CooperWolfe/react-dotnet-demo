using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Commands.Responses;
using Backend.Core.Model;
using Backend.Core.Transactions;

namespace Backend.Core.CommandHandlers;
internal class CreateRecipeCommandHandler : IRequestHandler<CreateRecipeCommand, CreateRecipeResponse>
{
    private readonly ILogger<CreateRecipeCommandHandler> logger;
    private readonly IBackendUnitOfWork unitOfWork;

    public CreateRecipeCommandHandler(
        ILogger<CreateRecipeCommandHandler> logger,
        IBackendUnitOfWork unitOfWork)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
    }

    public async Task<CreateRecipeResponse> Handle(CreateRecipeCommand command, CancellationToken cancellationToken)
    {
        logger.LogDebug("Creating recipe");
        var recipe = Recipe.Create(command);
        await unitOfWork.RecipeRepository.Create(recipe, cancellationToken);
        return new CreateRecipeResponse(createdRecipeId: recipe.Id);
    }
}