using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Transactions;

namespace Backend.Core.CommandHandlers;
internal class DeleteRecipeCommandHandler : IRequestHandler<DeleteRecipeCommand>
{
    private readonly ILogger<DeleteRecipeCommandHandler> logger;
    private readonly IBackendUnitOfWork backendUnitOfWork;
    private readonly IImageUnitOfWork imageUnitOfWork;

    public DeleteRecipeCommandHandler(
        ILogger<DeleteRecipeCommandHandler> logger,
        IBackendUnitOfWork backendUnitOfWork,
        IImageUnitOfWork imageUnitOfWork)
    {
        this.logger = logger;
        this.backendUnitOfWork = backendUnitOfWork;
        this.imageUnitOfWork = imageUnitOfWork;
    }

    public async Task<Unit> Handle(DeleteRecipeCommand command, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Deleting recipe with ID {command.Id}");
        await backendUnitOfWork.RecipeRepository.Delete(command.Id, cancellationToken);

        logger.LogDebug($"Deleting image of recipe '{command.Id}', if any");
        await imageUnitOfWork.ImageRepository.Delete(command.Id, cancellationToken);
        
        return Unit.Value;
    }
}