using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Transactions;

namespace Backend.Core.CommandHandlers;
internal class DeleteRecipeImageCommandHandler : IRequestHandler<DeleteRecipeImageCommand>
{
    private readonly ILogger<DeleteRecipeImageCommandHandler> logger;
    private readonly IImageUnitOfWork unitOfWork;

    public DeleteRecipeImageCommandHandler(
        ILogger<DeleteRecipeImageCommandHandler> logger,
        IImageUnitOfWork unitOfWork)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
    }

    public async Task<Unit> Handle(DeleteRecipeImageCommand command, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Deleting image for recipe '{command.Id}'");
        await unitOfWork.ImageRepository.Delete(command.Id, cancellationToken);
        return Unit.Value;
    }
}