using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Commands.Responses;
using Backend.Core.Repositories;

namespace Backend.Core.CommandHandlers;
internal class GetOneRecipeCommandHandler : IRequestHandler<GetOneRecipeCommand, GetOneRecipeResponse>
{
    private readonly ILogger<GetOneRecipeCommandHandler> logger;
    private readonly IRecipeRepository recipeRepository;

    public GetOneRecipeCommandHandler(
        ILogger<GetOneRecipeCommandHandler> logger,
        IRecipeRepository recipeRepository)
    {
        this.logger = logger;
        this.recipeRepository = recipeRepository;
    }

    public async Task<GetOneRecipeResponse> Handle(GetOneRecipeCommand command, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Reading recipe by ID {command.Id}");
        var recipe = await recipeRepository.ReadById(command.Id, cancellationToken);
        return new GetOneRecipeResponse(recipe);
    }
}