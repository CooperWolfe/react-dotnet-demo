using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Commands.Responses;
using Backend.Core.Repositories;
using Backend.Core.Exceptions;
using Microsoft.Extensions.Localization;

namespace Backend.Core.CommandHandlers;
internal class GetRecipeImageCommandHandler : IRequestHandler<GetRecipeImageCommand, GetRecipeImageResponse>
{
    private readonly ILogger<GetRecipeImageCommandHandler> logger;
    private readonly IRecipeImageRepository recipeImageRepository;
    private readonly IRecipeRepository recipeRepository;
    private readonly IStringLocalizer<Resources> localizer;

    public GetRecipeImageCommandHandler(
        ILogger<GetRecipeImageCommandHandler> logger,
        IRecipeImageRepository recipeImageRepository,
        IRecipeRepository recipeRepository,
        IStringLocalizer<Resources> localizer)
    {
        this.logger = logger;
        this.recipeImageRepository = recipeImageRepository;
        this.recipeRepository = recipeRepository;
        this.localizer = localizer;
    }

    public async Task<GetRecipeImageResponse> Handle(GetRecipeImageCommand command, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Checking if recipe '{command.Id}' exists");
        var recipe = await recipeRepository.ReadById(command.Id, cancellationToken);
        if (recipe is null)
        {
            throw new NotFoundException(localizer[Resources.NoSuchRecipePrompt, command.Id]);
        }

        logger.LogDebug($"Getting image for recipe '{command.Id}'");
        var recipeImage = await recipeImageRepository.GetImage(command.Id, cancellationToken);
        if (recipeImage is null)
        {
            logger.LogDebug($"No image found for recipe '{command.Id}' - using default");
            recipeImage = await recipeImageRepository.GetDefaultImage(cancellationToken);
        }
        return new GetRecipeImageResponse(type: recipeImage.Type, image: recipeImage.Image);
    }
}