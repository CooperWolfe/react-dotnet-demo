using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Commands.Responses;
using Backend.Core.Repositories;
using Backend.Core.Utilities;
using Microsoft.Extensions.Options;
using Backend.Core.Configuration;

namespace Backend.Core.CommandHandlers;
internal class SearchRecipesCommandHandler : IRequestHandler<SearchRecipesCommand, SearchRecipesResponse>
{
    private readonly ILogger<SearchRecipesCommandHandler> logger;
    private readonly IRecipeRepository recipeRepository;
    private readonly IRequestContext requestContext;
    private readonly IOptions<BackendConfiguration> options;

    public SearchRecipesCommandHandler(
        ILogger<SearchRecipesCommandHandler> logger,
        IRecipeRepository recipeRepository,
        IRequestContext requestContext,
        IOptions<BackendConfiguration> options)
    {
        this.logger = logger;
        this.recipeRepository = recipeRepository;
        this.requestContext = requestContext;
        this.options = options;
    }

    public async Task<SearchRecipesResponse> Handle(SearchRecipesCommand command, CancellationToken cancellationToken)
    {
        string? query = string.IsNullOrWhiteSpace(command.Query) ? null : command.Query;
        uint offset = command.Offset ?? 0u;
        uint limit = command.Limit ?? options.Value.DefaultRecipeSearchLimit;
        DateTime until = command.Until ?? requestContext.Timestamp;

        logger.LogDebug($"Searching recipes with query {query ?? "NO QUERY"}, offset {offset}, limit {limit}, until {until}");
        var recipes = await recipeRepository.Search(query, until, offset, limit, cancellationToken);

        return new SearchRecipesResponse(recipes: recipes);
    }
}