using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Transactions;
using Backend.Core.Repositories;
using Backend.Core.Exceptions;
using Microsoft.Extensions.Localization;

namespace Backend.Core.CommandHandlers;
internal class SetRecipeImageCommandHandler : IRequestHandler<SetRecipeImageCommand>
{
    private readonly ILogger<SetRecipeImageCommandHandler> logger;
    private readonly IRecipeRepository recipeRepository;
    private readonly IImageUnitOfWork unitOfWork;
    private readonly IStringLocalizer<Resources> localizer;

    public SetRecipeImageCommandHandler(
        ILogger<SetRecipeImageCommandHandler> logger,
        IRecipeRepository recipeRepository,
        IImageUnitOfWork unitOfWork,
        IStringLocalizer<Resources> localizer)
    {
        this.logger = logger;
        this.recipeRepository = recipeRepository;
        this.unitOfWork = unitOfWork;
        this.localizer = localizer;
    }

    public async Task<Unit> Handle(SetRecipeImageCommand command, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Checking if recipe '{command.RecipeId}' exists");
        var recipe = await recipeRepository.ReadById(command.RecipeId, cancellationToken);
        if (recipe is null)
        {
            throw new NotFoundException(localizer[Resources.NoSuchRecipePrompt, command.RecipeId]);
        }

        logger.LogDebug($"Setting image of type '{command.ImageType}' for recipe '{command.RecipeId}'");
        await unitOfWork.ImageRepository.SetRecipeImage(
            id: recipe.Id,
            type: command.ImageType,
            stream: command.Stream,
            cancellationToken);

        return Unit.Value;
    }
}