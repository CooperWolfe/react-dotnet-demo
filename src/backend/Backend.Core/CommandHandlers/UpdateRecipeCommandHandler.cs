using MediatR;
using Microsoft.Extensions.Logging;
using Backend.Core.Commands;
using Backend.Core.Transactions;
using Backend.Core.Model;
using Backend.Core.Exceptions;
using Microsoft.Extensions.Localization;

namespace Backend.Core.CommandHandlers;
internal class UpdateRecipeCommandHandler : IRequestHandler<UpdateRecipeCommand>
{
    private readonly ILogger<UpdateRecipeCommandHandler> logger;
    private readonly IBackendUnitOfWork unitOfWork;
    private readonly IStringLocalizer<Resources> localizer;

    public UpdateRecipeCommandHandler(
        ILogger<UpdateRecipeCommandHandler> logger,
        IBackendUnitOfWork unitOfWork,
        IStringLocalizer<Resources> localizer)
    {
        this.logger = logger;
        this.unitOfWork = unitOfWork;
        this.localizer = localizer;
    }

    public IStringLocalizer<Resources> Localizer => localizer;

    public async Task<Unit> Handle(UpdateRecipeCommand command, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Reading recipe by ID {command.Id}");
        var irecipe = await unitOfWork.RecipeRepository.ReadById(command.Id, cancellationToken);
        if (irecipe is null)
        {
            throw new NotFoundException(Localizer[Resources.NoSuchRecipePrompt, command.Id]);
        }

        logger.LogDebug($"Updating recipe with ID {command.Id}");
        var recipe = new Recipe(irecipe);
        recipe.Update(command);
        await unitOfWork.RecipeRepository.Update(recipe, cancellationToken);

        return Unit.Value;
    }
}