namespace Backend.Core.Commands;
public class CreateIngredientCommand
{
    public CreateIngredientCommand(string name, float amountValue, string? amountUnit)
    {
        Name = name;
        AmountValue = amountValue;
        AmountUnit = amountUnit;
    }

    public string Name { get; }
    public float AmountValue { get; }
    public string? AmountUnit { get; }
}