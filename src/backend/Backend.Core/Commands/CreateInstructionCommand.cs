namespace Backend.Core.Commands;
public class CreateInstructionCommand
{
    public CreateInstructionCommand(string description)
    {
        Description = description;
    }

    public string Description { get; }
}