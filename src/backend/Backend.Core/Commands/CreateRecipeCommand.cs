using MediatR;
using Backend.Core.Commands.Responses;

namespace Backend.Core.Commands;
public class CreateRecipeCommand : IRequest<CreateRecipeResponse>
{
    public CreateRecipeCommand(string name, string description, IEnumerable<CreateIngredientCommand> ingredients, IEnumerable<CreateInstructionCommand> instructions)
    {
        Name = name;
        Description = description;
        Ingredients = ingredients;
        Instructions = instructions;
    }

    public string Name { get; }
    public string Description { get; }
    public IEnumerable<CreateIngredientCommand> Ingredients { get; }
    public IEnumerable<CreateInstructionCommand> Instructions { get; }
}