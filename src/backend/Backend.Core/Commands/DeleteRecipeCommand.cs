using MediatR;

namespace Backend.Core.Commands;
public class DeleteRecipeCommand : IRequest
{
    public DeleteRecipeCommand(string id)
    {
        Id = id;
    }

    public string Id { get; }
}