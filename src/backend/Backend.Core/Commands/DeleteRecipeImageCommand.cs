using MediatR;

namespace Backend.Core.Commands;
public class DeleteRecipeImageCommand : IRequest
{
    public DeleteRecipeImageCommand(string id)
    {
        Id = id;
    }

    public string Id { get; }
}