using MediatR;
using Backend.Core.Commands.Responses;

namespace Backend.Core.Commands;
public class GetOneRecipeCommand : IRequest<GetOneRecipeResponse>
{
    public GetOneRecipeCommand(string id)
    {
        Id = id;
    }

    public string Id { get; }
}