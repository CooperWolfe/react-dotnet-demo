using MediatR;
using Backend.Core.Commands.Responses;

namespace Backend.Core.Commands;
public class GetRecipeImageCommand : IRequest<GetRecipeImageResponse>
{
    public GetRecipeImageCommand(string id)
    {
        Id = id;
    }

    public string Id { get; }
}