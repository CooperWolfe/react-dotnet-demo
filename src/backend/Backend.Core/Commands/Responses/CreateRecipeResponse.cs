namespace Backend.Core.Commands.Responses;
public class CreateRecipeResponse
{
    internal CreateRecipeResponse(string createdRecipeId)
    {
        CreatedRecipeId = createdRecipeId;
    }

    public string CreatedRecipeId { get; }
}