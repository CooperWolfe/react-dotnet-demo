using Backend.Core.Model.Read;

namespace Backend.Core.Commands.Responses;
public class GetOneRecipeResponse
{
    internal GetOneRecipeResponse(IRecipe? recipe)
    {
        Recipe = recipe;
    }

    public IRecipe? Recipe { get; }
}