using Backend.Core.Model;

namespace Backend.Core.Commands.Responses;
public class GetRecipeImageResponse
{
    internal GetRecipeImageResponse(ImageType type, Stream image)
    {
        Type = type;
        Image = image;
    }

    public ImageType Type { get; }
    public Stream Image { get; }
}