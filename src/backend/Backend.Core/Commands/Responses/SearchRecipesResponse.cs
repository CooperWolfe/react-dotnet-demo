using Backend.Core.Model.Read;

namespace Backend.Core.Commands.Responses;
public class SearchRecipesResponse
{
    internal SearchRecipesResponse(IEnumerable<IRecipe> recipes)
    {
        Recipes = recipes;
    }

    public IEnumerable<IRecipe> Recipes { get; }
}