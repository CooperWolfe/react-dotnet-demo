using MediatR;
using Backend.Core.Commands.Responses;

namespace Backend.Core.Commands;
public class SearchRecipesCommand : IRequest<SearchRecipesResponse>
{
    public SearchRecipesCommand(string? query, uint? offset, uint? limit, DateTime? until)
    {
        Query = query;
        Offset = offset;
        Limit = limit;
        Until = until;
    }

    public string? Query { get; }
    public uint? Offset { get; }
    public uint? Limit { get; }
    public DateTime? Until { get; }
}