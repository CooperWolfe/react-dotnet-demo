using Backend.Core.Model;
using MediatR;

namespace Backend.Core.Commands;
public class SetRecipeImageCommand : IRequest
{
    public SetRecipeImageCommand(string recipeId, ImageType imageType, Stream stream)
    {
        RecipeId = recipeId;
        ImageType = imageType;
        Stream = stream;
    }

    public string RecipeId { get; }
    public ImageType ImageType { get; }
    public Stream Stream { get; }
}