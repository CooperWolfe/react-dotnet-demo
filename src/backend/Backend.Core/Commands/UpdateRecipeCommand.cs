using MediatR;
using Backend.Core.Commands.Responses;

namespace Backend.Core.Commands;
public class UpdateRecipeCommand : IRequest
{
    public UpdateRecipeCommand(string id, string name, string description, IEnumerable<CreateIngredientCommand> ingredients, IEnumerable<CreateInstructionCommand> instructions)
    {
        Id = id;
        Name = name;
        Description = description;
        Ingredients = ingredients;
        Instructions = instructions;
    }

    public string Id { get; }
    public string Name { get; }
    public string Description { get; }
    public IEnumerable<CreateIngredientCommand> Ingredients { get; }
    public IEnumerable<CreateInstructionCommand> Instructions { get; }
}