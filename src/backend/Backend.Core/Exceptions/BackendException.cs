namespace Backend.Core.Exceptions;
[Serializable]
public abstract class BackendException : Exception
{
    public BackendException() { }
    public BackendException(string message) : base(message) { }
    public BackendException(string message, Exception inner) : base(message, inner) { }
}