using FluentValidation.Results;

namespace Backend.Core.Exceptions;
[Serializable]
public class InvalidCommandException : Core.Exceptions.ValidationException
{
    public InvalidCommandException(ValidationResult result) : base(@"The request was invalid")
    {
        ValidationResult = result;
    }

    public ValidationResult ValidationResult { get; }
}