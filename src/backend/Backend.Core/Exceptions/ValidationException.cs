namespace Backend.Core.Exceptions;
[Serializable]
public class ValidationException : BackendException
{
    public ValidationException() : base() { }
    public ValidationException(string message) : base(message) { }
    public ValidationException(string message, Exception innerException) : base(message, innerException) { }
}