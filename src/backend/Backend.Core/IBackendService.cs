using Backend.Core.Commands;
using Backend.Core.Commands.Responses;

namespace Backend.Core;
public interface IBackendService
{
    Task<CreateRecipeResponse> CreateRecipe(CreateRecipeCommand command, CancellationToken cancellationToken);
    Task DeleteRecipe(DeleteRecipeCommand command, CancellationToken cancellationToken);
    Task<GetOneRecipeResponse> GetOneRecipe(GetOneRecipeCommand command, CancellationToken cancellationToken);
    Task<GetRecipeImageResponse> GetRecipeImage(GetRecipeImageCommand command, CancellationToken cancellationToken);
    Task<SearchRecipesResponse> SearchRecipes(SearchRecipesCommand command, CancellationToken cancellationToken);
    Task SetRecipeImage(SetRecipeImageCommand command, CancellationToken cancellationToken);
    Task UpdateRecipe(UpdateRecipeCommand command, CancellationToken cancellationToken);
    Task DeleteRecipeImage(DeleteRecipeImageCommand command, CancellationToken cancellationToken);
}