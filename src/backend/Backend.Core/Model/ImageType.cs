namespace Backend.Core.Model;
public enum ImageType
{
    JPEG,
    PNG,
}