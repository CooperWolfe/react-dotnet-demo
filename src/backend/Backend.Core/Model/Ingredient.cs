using Backend.Core.Commands;
using Backend.Core.Model.Read;

namespace Backend.Core.Model;
public class Ingredient
{
    private Ingredient(string name, float amountValue, string? amountUnit)
    {
        Name = name;
        AmountValue = amountValue;
        AmountUnit = amountUnit;
    }
    internal Ingredient(IIngredient ingredient)
    {
        Name = ingredient.Name.Trim();
        AmountValue = ingredient.AmountValue;
        AmountUnit = string.IsNullOrWhiteSpace(ingredient.AmountUnit) ? null : ingredient.AmountUnit;
    }

    public string Name { get; }
    public float AmountValue { get; }
    public string? AmountUnit { get; }

    internal static Ingredient Create(CreateIngredientCommand command)
    {
        return new Ingredient(
            name: command.Name,
            amountValue: command.AmountValue,
            amountUnit: command.AmountUnit);
    }
}