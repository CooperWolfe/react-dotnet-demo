using Backend.Core.Commands;
using Backend.Core.Model.Read;

namespace Backend.Core.Model;
public class Instruction
{
    private Instruction(string description)
    {
        Description = description;
    }
    internal Instruction(IInstruction instruction)
    {
        Description = instruction.Description;
    }

    public string Description { get; }

    internal static Instruction Create(CreateInstructionCommand command)
    {
        return new Instruction(command.Description);
    }
}