namespace Backend.Core.Model.Read;
public interface IIngredient
{
    string Name { get; }
    float AmountValue { get; }
    string? AmountUnit { get; }
}