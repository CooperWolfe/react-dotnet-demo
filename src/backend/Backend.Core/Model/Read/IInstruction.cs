namespace Backend.Core.Model.Read;
public interface IInstruction
{
    string Description { get; }
}