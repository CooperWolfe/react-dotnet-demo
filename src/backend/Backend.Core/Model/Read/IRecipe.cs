namespace Backend.Core.Model.Read;
public interface IRecipe
{
    string Id { get; }
    string Name { get; }
    string Description { get; }
    IEnumerable<IIngredient> Ingredients { get; }
    IEnumerable<IInstruction> Instructions { get; }
    DateTime CreatedAt { get; }
}