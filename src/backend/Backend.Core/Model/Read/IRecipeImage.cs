namespace Backend.Core.Model.Read;
public interface IRecipeImage
{
    ImageType Type { get; }
    Stream Image { get; }
}