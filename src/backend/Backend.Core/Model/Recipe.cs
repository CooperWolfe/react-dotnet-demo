using Backend.Core.Commands;
using Backend.Core.Model.Read;

namespace Backend.Core.Model;
public class Recipe
{
    private Recipe(string name, string description, IEnumerable<Ingredient> ingredients, IEnumerable<Instruction> instructions)
    {
        Id = Guid.NewGuid().ToString();
        Name = name;
        Description = description;
        Ingredients = ingredients;
        Instructions = instructions;
        CreatedAt = DateTime.UtcNow;
    }
    internal Recipe(IRecipe recipe)
    {
        Id = recipe.Id;
        Name = recipe.Name;
        Description = recipe.Description;
        Ingredients = recipe.Ingredients.Select(i => new Ingredient(i));
        Instructions = recipe.Instructions.Select(i => new Instruction(i));
        CreatedAt = recipe.CreatedAt;
    }

    public string Id { get; }

    private string name = "";
    public string Name
    {
        get => name;
        private set => name = value.Trim();
    }

    private string description = "";
    public string Description
    {
        get => description;
        private set => description = value.Trim();
    }
    
    public IEnumerable<Ingredient> Ingredients { get; private set; }
    public IEnumerable<Instruction> Instructions { get; private set; }
    public DateTime CreatedAt { get; }

    internal static Recipe Create(CreateRecipeCommand command)
    {
        return new Recipe(
            name: command.Name,
            description: command.Description,
            ingredients: command.Ingredients.Select(Ingredient.Create),
            instructions: command.Instructions.Select(Instruction.Create));
    }
    internal void Update(UpdateRecipeCommand command)
    {
        Name = command.Name;
        Description = command.Description;
        Ingredients = command.Ingredients.Select(Ingredient.Create);
        Instructions = command.Instructions.Select(Instruction.Create);
    }
}