using Backend.Core.Model;
using Backend.Core.Model.Read;

namespace Backend.Core.Repositories;
public interface IRecipeImageRepository
{
    Task Delete(string id, CancellationToken cancellationToken);
    Task<IRecipeImage> GetDefaultImage(CancellationToken cancellationToken);
    Task<IRecipeImage?> GetImage(string id, CancellationToken cancellationToken);
    Task SetRecipeImage(string id, ImageType type, Stream stream, CancellationToken cancellationToken);
}