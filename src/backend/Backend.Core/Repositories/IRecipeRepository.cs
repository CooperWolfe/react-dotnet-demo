using Backend.Core.Model;
using Backend.Core.Model.Read;

namespace Backend.Core.Repositories;
public interface IRecipeRepository
{
    /// <returns>The ID of the created recipe</returns>
    Task Create(Recipe recipe, CancellationToken cancellationToken);
    Task Delete(string id, CancellationToken cancellationToken);
    Task<IRecipe?> ReadById(string id, CancellationToken cancellationToken);
    Task<IEnumerable<IRecipe>> Search(string? query, DateTime until, uint offset, uint limit, CancellationToken cancellationToken);
    Task Update(Recipe recipe, CancellationToken cancellationToken);
}