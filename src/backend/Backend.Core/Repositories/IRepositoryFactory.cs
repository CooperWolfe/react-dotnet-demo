using Backend.Core.Transactions;

namespace Backend.Core.Repositories;
public interface IRepositoryFactory<TRepository>
{
    TRepository Create(ITransaction transaction);
}