namespace Backend.Core;
public class Resources
{
    /// <summary>
    /// {0} = recipe ID
    /// </summary>
    public const string NoSuchRecipePrompt = nameof(NoSuchRecipePrompt);
    public const string NonSpaceWhitespaceError = nameof(NonSpaceWhitespaceError);
    public const string NoWhitespaceError = nameof(NoWhitespaceError);
    /// <summary>
    /// {0} = one of what
    /// </summary>
    public const string AtLeastOneError = nameof(AtLeastOneError);
    /// <summary>
    /// {0} = what cannot exceed int.MaxValue
    /// </summary>
    public const string ValueOverIntMaxValueError = nameof(ValueOverIntMaxValueError);
}