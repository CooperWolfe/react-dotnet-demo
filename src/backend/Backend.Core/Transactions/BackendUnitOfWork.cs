using Backend.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace Backend.Core.Transactions;
internal class BackendUnitOfWork : UnitOfWork, IBackendUnitOfWork
{
    private readonly ITransaction transaction;
    private readonly IRepositoryFactory<IRecipeRepository> recipeRepositoryFactory;

    public BackendUnitOfWork(
        ILogger<BackendUnitOfWork> logger,
        ITransaction transaction,
        IRepositoryFactory<IRecipeRepository> recipeRepositoryFactory) : base(logger, transaction)
    {
        this.transaction = transaction;
        this.recipeRepositoryFactory = recipeRepositoryFactory;
    }

    private IRecipeRepository? recipeRepository;
    public IRecipeRepository RecipeRepository => recipeRepository ??= recipeRepositoryFactory.Create(transaction);

    protected override IEnumerable<object?> GetRepositories()
    {
        yield return recipeRepository;
    }
}