using Backend.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace Backend.Core.Transactions;
internal class BackendUnitOfWorkFactory : IUnitOfWorkFactory<IBackendUnitOfWork>
{
    private readonly ILoggerFactory loggerFactory;
    private readonly ITransactionFactory<IBackendTransaction> transactionFactory;
    private readonly IRepositoryFactory<IRecipeRepository> recipeRepositoryFactory;

    public BackendUnitOfWorkFactory(
        ILoggerFactory loggerFactory,
        ITransactionFactory<IBackendTransaction> transactionFactory,
        IRepositoryFactory<IRecipeRepository> recipeRepositoryFactory)
    {
        this.loggerFactory = loggerFactory;
        this.transactionFactory = transactionFactory;
        this.recipeRepositoryFactory = recipeRepositoryFactory;
    }

    public IBackendUnitOfWork? Current { get; private set; } = null;

    public IBackendUnitOfWork Create()
    {
        if (Current != null)
        {
            throw new InvalidOperationException("Do not try to create a unit of work within the scope of an existing unit of work.");
        }

        return Current = new BackendUnitOfWork(
            logger: loggerFactory.CreateLogger<BackendUnitOfWork>(),
            transaction: transactionFactory.Create(),
            recipeRepositoryFactory: recipeRepositoryFactory)
        {
            OnDispose = () => Current = null
        };
    }
}
