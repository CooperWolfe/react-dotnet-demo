using Backend.Core.Repositories;

namespace Backend.Core.Transactions;
internal interface IBackendUnitOfWork : IUnitOfWork
{
    IRecipeRepository RecipeRepository { get; }
}