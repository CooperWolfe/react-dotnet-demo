using Backend.Core.Repositories;

namespace Backend.Core.Transactions;
internal interface IImageUnitOfWork : IUnitOfWork
{
    IRecipeImageRepository ImageRepository { get; }
}