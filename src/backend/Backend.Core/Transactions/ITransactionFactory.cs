namespace Backend.Core.Transactions;
public interface ITransactionFactory<TTransaction> where TTransaction : ITransaction
{
    TTransaction Create();
}
