namespace Backend.Core.Transactions;
internal interface IUnitOfWork : IDisposable, IAsyncDisposable
{
    Task CommitAsync(CancellationToken cancellationToken = default);
    Task RollbackAsync();
}