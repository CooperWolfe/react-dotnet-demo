namespace Backend.Core.Transactions;
internal interface IUnitOfWorkFactory<TUnitOfWork> where TUnitOfWork : IUnitOfWork
{
    TUnitOfWork? Current { get; }
    TUnitOfWork Create();
}