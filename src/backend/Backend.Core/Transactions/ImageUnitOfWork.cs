using Backend.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace Backend.Core.Transactions;
internal class ImageUnitOfWork : UnitOfWork, IImageUnitOfWork
{
    private readonly ITransaction transaction;
    private readonly IRepositoryFactory<IRecipeImageRepository> recipeImageRepositoryFactory;

    public ImageUnitOfWork(
        ILogger<ImageUnitOfWork> logger,
        ITransaction transaction,
        IRepositoryFactory<IRecipeImageRepository> recipeImageRepositoryFactory) : base(logger, transaction)
    {
        this.transaction = transaction;
        this.recipeImageRepositoryFactory = recipeImageRepositoryFactory;
    }

    private IRecipeImageRepository? recipeImageRepository = null;
    public IRecipeImageRepository ImageRepository => recipeImageRepository ??= recipeImageRepositoryFactory.Create(transaction);

    protected override IEnumerable<object?> GetRepositories()
    {
        yield return recipeImageRepository;
    }
}