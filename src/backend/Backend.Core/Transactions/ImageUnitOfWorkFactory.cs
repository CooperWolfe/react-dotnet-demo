using Backend.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace Backend.Core.Transactions;
internal class ImageUnitOfWorkFactory : IUnitOfWorkFactory<IImageUnitOfWork>
{
    private readonly ILoggerFactory loggerFactory;
    private readonly ITransactionFactory<IImageTransaction> transactionFactory;
    private readonly IRepositoryFactory<IRecipeImageRepository> recipeImageRepositoryFactory;

    public ImageUnitOfWorkFactory(
        ILoggerFactory loggerFactory,
        ITransactionFactory<IImageTransaction> transactionFactory,
        IRepositoryFactory<IRecipeImageRepository> recipeImageRepositoryFactory)
    {
        this.loggerFactory = loggerFactory;
        this.transactionFactory = transactionFactory;
        this.recipeImageRepositoryFactory = recipeImageRepositoryFactory;
    }

    public IImageUnitOfWork? Current { get; private set; } = null;

    public IImageUnitOfWork Create()
    {
        if (Current != null)
        {
            throw new InvalidOperationException("Do not try to create a unit of work within the scope of an existing unit of work.");
        }

        return Current = new ImageUnitOfWork(
            logger: loggerFactory.CreateLogger<ImageUnitOfWork>(),
            transaction: transactionFactory.Create(),
            recipeImageRepositoryFactory: recipeImageRepositoryFactory)
        {
            OnDispose = () => Current = null
        };
    }
}
