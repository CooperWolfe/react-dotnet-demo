using Microsoft.Extensions.Logging;

namespace Backend.Core.Transactions;
internal abstract class UnitOfWork : IUnitOfWork
{
    private readonly ILogger<UnitOfWork> logger;
    private readonly ITransaction transaction;

    private bool isCompleted = false;

    public UnitOfWork(
        ILogger<UnitOfWork> logger,
        ITransaction transaction)
    {
        this.logger = logger;
        this.transaction = transaction;
    }

    public Action? OnDispose { get; set; }

    protected abstract IEnumerable<object?> GetRepositories();

    public async Task CommitAsync(CancellationToken cancellationToken = default)
    {
        logger.LogDebug("Committing unit of work");
        isCompleted = true;
        await transaction.CommitAsync(cancellationToken);
    }

    public void Dispose()
    {
        DisposeAsync().AsTask().Wait();
    }
    public virtual async ValueTask DisposeAsync()
    {
        if (!isCompleted)
        {
            await RollbackAsync();
        }

        await transaction.DisposeAsync();
        OnDispose?.Invoke();

        foreach (var repository in GetRepositories())
        {
            if (repository == null) continue;
            if (repository is IAsyncDisposable asyncDisposable) await asyncDisposable.DisposeAsync();
            else if (repository is IDisposable disposable) disposable.Dispose();
        }
    }

    public async Task RollbackAsync()
    {
        logger.LogDebug("Rolling back unit of work");
        isCompleted = true;
        await transaction.RollbackAsync();
    }
}
