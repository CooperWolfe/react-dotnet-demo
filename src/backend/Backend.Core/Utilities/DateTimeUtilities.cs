namespace Backend.Core.Utilities;
internal static class DateTimeUtilities
{
    public const double MaxAllowedSkewInMinutes = 10d;
    public const double MaxPositiveTimezoneOffset = 14d;
    
    /// <param name="dateTime">Must be UTC</param>
    /// <returns>The dateTime, offset using the maximum possible timezone and maximum allowed skew</returns>
    public static DateTime LatestPossible(this DateTime dateTime)
    {
        return dateTime
            .AddMinutes(MaxAllowedSkewInMinutes)
            .AddHours(MaxPositiveTimezoneOffset);
    }
}