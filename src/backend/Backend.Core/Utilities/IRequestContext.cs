namespace Backend.Core.Utilities;
internal interface IRequestContext
{
    DateTime Timestamp { get; }
}