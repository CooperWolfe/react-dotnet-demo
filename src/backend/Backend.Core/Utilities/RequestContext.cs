namespace Backend.Core.Utilities;
internal class RequestContext : IRequestContext
{
    public RequestContext()
    {
        Timestamp = DateTime.UtcNow;
    }

    public DateTime Timestamp { get; }
}