using Backend.Core.Commands;
using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Backend.Core.Validation;
internal class CreateIngredientCommandValidator : AbstractValidator<CreateIngredientCommand>
{
    public CreateIngredientCommandValidator(IStringLocalizer localizer)
    {
        RuleFor(cmd => cmd.Name)
            .DisallowNonSpaceWhitespace()
            .WithMessage(_ => localizer[Resources.NonSpaceWhitespaceError]);
        RuleFor(cmd => cmd.Name)
            .MaximumLength(65535)
            .NotEmpty();
        RuleFor(cmd => cmd.AmountValue)
            .GreaterThan(0f)
            .LessThan(10000f);
        RuleFor(cmd => cmd.AmountUnit)
            .Matches(@"^\S*$")
            .WithMessage(_ => localizer[Resources.NoWhitespaceError]);
        RuleFor(cmd => cmd.AmountUnit)
            .MaximumLength(5);
    }
}