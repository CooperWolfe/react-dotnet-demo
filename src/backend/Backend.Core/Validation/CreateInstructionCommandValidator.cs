using Backend.Core.Commands;
using FluentValidation;

namespace Backend.Core.Validation;
internal class CreateInstructionCommandValidator : AbstractValidator<CreateInstructionCommand>
{
    public CreateInstructionCommandValidator()
    {
        RuleFor(cmd => cmd.Description)
            .MaximumLength(65535)
            .NotEmpty();
    }
}