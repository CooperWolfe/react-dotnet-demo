using Backend.Core.Commands;
using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Backend.Core.Validation;
internal class CreateRecipeCommandValidator : AbstractValidator<CreateRecipeCommand>
{
    public CreateRecipeCommandValidator(IStringLocalizer<Resources> localizer)
    {
        RuleFor(cmd => cmd.Name)
            .SetValidator(new RecipeNameValidator(localizer))
            .NotEmpty();
        RuleFor(cmd => cmd.Description)
            .SetValidator(new RecipeDescriptionValidator(localizer))
            .NotEmpty();
        RuleFor(cmd => cmd.Ingredients.Count())
            .GreaterThan(0)
            .WithMessage(_ => localizer[Resources.AtLeastOneError, nameof(CreateRecipeCommand.Ingredients)]);
        RuleFor(cmd => cmd.Instructions.Count())
            .GreaterThan(0)
            .WithMessage(_ => localizer[Resources.AtLeastOneError, nameof(CreateRecipeCommand.Instructions)]);
        RuleForEach(cmd => cmd.Ingredients).SetValidator(new CreateIngredientCommandValidator(localizer));
        RuleForEach(cmd => cmd.Instructions).SetValidator(new CreateInstructionCommandValidator());
    }
}