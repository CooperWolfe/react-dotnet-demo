using Backend.Core.Commands;
using FluentValidation;

namespace Backend.Core.Validation;
internal class DeleteRecipeCommandValidator : AbstractValidator<DeleteRecipeCommand>
{
    public DeleteRecipeCommandValidator()
    {
        RuleFor(cmd => cmd.Id).NotEmpty();
    }
}