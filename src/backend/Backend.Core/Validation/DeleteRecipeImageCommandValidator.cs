using Backend.Core.Commands;
using FluentValidation;

namespace Backend.Core.Validation;
internal class DeleteRecipeImageCommandValidator : AbstractValidator<DeleteRecipeImageCommand>
{
    public DeleteRecipeImageCommandValidator()
    {
        RuleFor(cmd => cmd.Id).NotEmpty();
    }
}