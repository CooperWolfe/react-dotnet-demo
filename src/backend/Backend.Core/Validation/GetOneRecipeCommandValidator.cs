using Backend.Core.Commands;
using FluentValidation;

namespace Backend.Core.Validation;
internal class GetOneRecipeCommandValidator : AbstractValidator<GetOneRecipeCommand>
{
    public GetOneRecipeCommandValidator()
    {
        RuleFor(cmd => cmd.Id).NotEmpty();
    }
}