using Backend.Core.Commands;
using FluentValidation;

namespace Backend.Core.Validation;
internal class GetRecipeImageCommandValidator : AbstractValidator<GetRecipeImageCommand>
{
    public GetRecipeImageCommandValidator()
    {
        RuleFor(cmd => cmd.Id).NotEmpty();
    }
}