using FluentValidation.Results;

namespace Backend.Core.Validation.Mediator;
internal interface ICommandValidator
{
    ValidationResult Validate(object obj);
    /// <throws cref="Core.Exceptions.InvalidCommandException" />
    void ValidateAndThrow(object obj);
}