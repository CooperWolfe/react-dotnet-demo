using Backend.Core.Exceptions;
using FluentValidation;
using FluentValidation.Results;

namespace Backend.Core.Validation.Mediator;
internal class MediatorCommandValidator : ICommandValidator
{
    private readonly IServiceProvider serviceProvider;

    public MediatorCommandValidator(IServiceProvider serviceProvider)
    {
        this.serviceProvider = serviceProvider;
    }

    public ValidationResult Validate(object obj)
    {
        var objType = obj.GetType();
        var genericValidatorType = typeof(IValidator<>).MakeGenericType(objType);
        var validator = serviceProvider.GetService(genericValidatorType);
        if (validator == null)
        {
            throw new InvalidOperationException("No such validator. Register your validator to DI in Startup.cs.");
        }
        var result = validator.GetType().GetMethod(nameof(IValidator<object>.Validate), new[] { objType })!.Invoke(validator, new[] { obj })!;
        return (ValidationResult)result;
    }

    public void ValidateAndThrow(object obj)
    {
        var result = Validate(obj);
        if (!result.IsValid)
        {
            throw new InvalidCommandException(result);
        }
    }
}