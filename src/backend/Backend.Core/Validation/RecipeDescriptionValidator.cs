using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Backend.Core.Validation;
internal class RecipeDescriptionValidator : AbstractValidator<string>
{
    public RecipeDescriptionValidator(IStringLocalizer localizer)
    {
        RuleFor(s => s)
            .DisallowNonSpaceWhitespace()
            .WithMessage(_ => localizer[Resources.NonSpaceWhitespaceError]);
        RuleFor(s => s)
            .MaximumLength(65535);
    }
}