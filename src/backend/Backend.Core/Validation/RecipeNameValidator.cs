using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Backend.Core.Validation;
internal class RecipeNameValidator : AbstractValidator<string>
{
    public RecipeNameValidator(IStringLocalizer localizer)
    {
        RuleFor(s => s).MaximumLength(50);
        RuleFor(s => s)
            .DisallowNonSpaceWhitespace()
            .WithMessage(_ => localizer[Resources.NonSpaceWhitespaceError]);
    }
}