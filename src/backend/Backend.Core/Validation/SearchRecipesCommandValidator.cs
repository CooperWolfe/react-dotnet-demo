using Backend.Core.Commands;
using Backend.Core.Utilities;
using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Backend.Core.Validation;
internal class SearchRecipesCommandValidator : AbstractValidator<SearchRecipesCommand>
{
    public SearchRecipesCommandValidator(
        IRequestContext requestContext,
        IStringLocalizer<Resources> localizer)
    {
        RuleFor(cmd => cmd.Limit)
            .GreaterThan(0u)
            .LessThanOrEqualTo(1000u);
        RuleFor(cmd => cmd.Offset)
            .LessThanOrEqualTo(Convert.ToUInt32(int.MaxValue))
            .WithMessage(_ => localizer[Resources.ValueOverIntMaxValueError, nameof(SearchRecipesCommand.Offset)]);
        RuleFor(cmd => cmd.Until)
            .LessThanOrEqualTo(requestContext.Timestamp.LatestPossible());
    }
}