using Backend.Core.Commands;
using FluentValidation;

namespace Backend.Core.Validation;
internal class SetRecipeImageCommandValidator : AbstractValidator<SetRecipeImageCommand>
{
    public SetRecipeImageCommandValidator()
    {
        RuleFor(cmd => cmd.RecipeId).NotEmpty();
        RuleFor(cmd => cmd.ImageType).IsInEnum();
        RuleFor(cmd => cmd.Stream.Length).GreaterThan(0);
    }
}