using Backend.Core.Commands;
using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Backend.Core.Validation;
internal class UpdateRecipeCommandValidator : AbstractValidator<UpdateRecipeCommand>
{
    public UpdateRecipeCommandValidator(IStringLocalizer<Resources> localizer)
    {
        RuleFor(cmd => cmd.Id).NotEmpty();
        RuleFor(cmd => cmd.Name)
            .SetValidator(new RecipeNameValidator(localizer))
            .NotEmpty();
        RuleFor(cmd => cmd.Description)
            .SetValidator(new RecipeDescriptionValidator(localizer))
            .NotEmpty();
        RuleFor(cmd => cmd.Ingredients.Count())
            .GreaterThan(0)
            .WithMessage(_ => localizer[Resources.AtLeastOneError, nameof(UpdateRecipeCommand.Ingredients)]);
        RuleFor(cmd => cmd.Instructions.Count())
            .GreaterThan(0)
            .WithMessage(_ => localizer[Resources.AtLeastOneError, nameof(UpdateRecipeCommand.Instructions)]);
        RuleForEach(cmd => cmd.Ingredients).SetValidator(new CreateIngredientCommandValidator(localizer));
        RuleForEach(cmd => cmd.Instructions).SetValidator(new CreateInstructionCommandValidator());
    }
}