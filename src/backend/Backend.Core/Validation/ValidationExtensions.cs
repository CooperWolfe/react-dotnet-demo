using FluentValidation;

namespace Backend.Core.Validation;
internal static class ValidationExtensions
{
    public static IRuleBuilderOptions<T, string> DisallowNonSpaceWhitespace<T>(this IRuleBuilderInitial<T, string> options)
    {
        return options.Matches(@"^(\S|(?<! ) )*$");
    }
}