namespace Backend.FileSystem.Commands;
internal class DeleteDirectoryCommand : ICommand
{
    private readonly string directoryName;

    public DeleteDirectoryCommand(string directoryName)
    {
        this.directoryName = directoryName;
    }

    public Task ExecuteAsync(CancellationToken cancellationToken)
    {
        if (Directory.Exists(directoryName))
            Directory.Delete(directoryName, recursive: true);
        return Task.CompletedTask;
    }
    public Task PrepareAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    public Task RollbackAsync() => Task.CompletedTask;
    public void Dispose() { }
    public ValueTask DisposeAsync() => ValueTask.CompletedTask;
}