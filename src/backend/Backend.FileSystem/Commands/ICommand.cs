namespace Backend.FileSystem.Commands;
internal interface ICommand : IDisposable, IAsyncDisposable
{
    Task PrepareAsync(CancellationToken cancellationToken);
    Task ExecuteAsync(CancellationToken cancellationToken);
    Task RollbackAsync();
}