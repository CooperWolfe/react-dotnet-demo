using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;

namespace Backend.FileSystem.Commands;
internal class SetJpegCommand : ICommand
{
    private readonly string transactionId;
    private readonly Stream stream;
    private readonly string recipeRootPath;
    private readonly string txRootPath;
    private readonly string tmpFilePath;
    private readonly string filePath;

    public SetJpegCommand(string transactionId, string path, Stream stream)
    {
        this.transactionId = transactionId;
        this.stream = stream;
        this.recipeRootPath = Path.GetDirectoryName(path)!;
        this.txRootPath = Path.Combine(recipeRootPath, $".tx-{transactionId}");
        this.tmpFilePath = Path.Combine(txRootPath, Path.GetFileNameWithoutExtension(path) + ".jpg");
        this.filePath = Path.Combine(recipeRootPath, Path.GetFileNameWithoutExtension(path) + ".jpg");
    }

    public Task PrepareAsync(CancellationToken cancellationToken)
    {
        var recipeRoot = new DirectoryInfo(recipeRootPath);
        if (!recipeRoot.Exists) recipeRoot.Create();
        var txRoot = new DirectoryInfo(txRootPath);
        txRoot.Create();
        using (var image = Image.Load(stream))
            using (var fileStream = File.OpenWrite(tmpFilePath))
                image.Save(fileStream, new JpegEncoder { Quality = 100 });
        return Task.CompletedTask;
    }
    public Task ExecuteAsync(CancellationToken cancellationToken)
    {
        File.Move(tmpFilePath, filePath, overwrite: true);
        return Task.CompletedTask;
    }
    public Task RollbackAsync()
    {
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        try
        {
            Directory.Delete(txRootPath, recursive: true);
        }
        catch (DirectoryNotFoundException) { } // swallow
    }
    public ValueTask DisposeAsync()
    {
        Dispose();
        return ValueTask.CompletedTask;
    }
}
