namespace Backend.FileSystem.Configuration;
public class FileSystemConfiguration
{
    private string rootDirectory = "";
    public string RootDirectory
    {
        get => rootDirectory;
        set => rootDirectory = value.TrimEnd('/');
    }
}