﻿using Backend.Core.Repositories;
using Backend.Core.Transactions;
using Backend.FileSystem.Configuration;
using Backend.FileSystem.Repositories;
using Backend.FileSystem.Transactions;
using Microsoft.Extensions.Options;

namespace Microsoft.Extensions.DependencyInjection;
public static class FileSystemServiceCollectionExtensions
{
    public static void AddFileSystem(this IServiceCollection services, Action<OptionsBuilder<FileSystemConfiguration>> configure)
    {
        configure(services.AddOptions<FileSystemConfiguration>());

        services.AddTransient<ITransactionFactory<IImageTransaction>, FileSystemTransactionFactory>();

        services.AddTransient<IRepositoryFactory<IRecipeImageRepository>, FileSystemRecipeImageRepositoryFactory>();
        services.AddScoped<IRecipeImageRepository, FileSystemRecipeImageRepository>(sp => new FileSystemRecipeImageRepository(
            transaction: null,
            options: sp.GetRequiredService<IOptions<FileSystemConfiguration>>()));
    }
}
