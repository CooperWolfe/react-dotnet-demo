using Backend.Core.Model;
using Backend.Core.Model.Read;

namespace Backend.FileSystem.Model;
internal class FileSystemRecipeImage : IRecipeImage
{
    public FileSystemRecipeImage(ImageType type, Stream image)
    {
        Type = type;
        Image = image;
    }

    public ImageType Type { get; set; }
    public Stream Image { get; set; }
}
