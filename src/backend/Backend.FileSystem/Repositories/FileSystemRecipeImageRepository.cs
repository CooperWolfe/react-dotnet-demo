using Backend.Core.Model;
using Backend.Core.Model.Read;
using Backend.Core.Repositories;
using Backend.FileSystem.Commands;
using Backend.FileSystem.Configuration;
using Backend.FileSystem.Model;
using Backend.FileSystem.Transactions;
using Microsoft.Extensions.Options;

namespace Backend.FileSystem.Repositories;
internal class FileSystemRecipeImageRepository : IRecipeImageRepository
{
    private readonly FileSystemTransaction? transaction;
    private readonly IOptions<FileSystemConfiguration> options;

    public FileSystemRecipeImageRepository(
        FileSystemTransaction? transaction,
        IOptions<FileSystemConfiguration> options)
    {
        this.transaction = transaction;
        this.options = options;
    }

    public async Task Delete(string id, CancellationToken cancellationToken)
    {
        if (transaction == null) throw new InvalidOperationException("Cannot delete recipe image outside of a transaction");
        string recipeImagePath = RecipeImagePath(id);
        var command = new DeleteDirectoryCommand(Path.GetDirectoryName(recipeImagePath)!);
        await transaction.ExecuteAsync(command, cancellationToken);
    }

    public Task<IRecipeImage> GetDefaultImage(CancellationToken cancellationToken)
    {
        string defaultImagePath = Path.Combine(options.Value.RootDirectory, "default.jpg");
        var defaultImageInfo = new FileInfo(defaultImagePath);
        var recipeImage = new FileSystemRecipeImage(type: ImageType.JPEG, image: defaultImageInfo.OpenRead());
        return Task.FromResult<IRecipeImage>(recipeImage);
    }

    public Task<IRecipeImage?> GetImage(string id, CancellationToken cancellationToken)
    {
        string recipeImagePath = RecipeImagePath(id: id);
        var recipeRoot = new DirectoryInfo(Path.GetDirectoryName(recipeImagePath)!);
        if (!recipeRoot.Exists) return Task.FromResult<IRecipeImage?>(null);
        var recipeImageInfo = recipeRoot.GetFiles().SingleOrDefault();
        var recipeImage = recipeImageInfo == null
            ? null
            : new FileSystemRecipeImage(type: ImageType.JPEG, image: recipeImageInfo.OpenRead());
        return Task.FromResult<IRecipeImage?>(recipeImage);
    }

    public async Task SetRecipeImage(string id, ImageType type, Stream stream, CancellationToken cancellationToken)
    {
        if (transaction == null) throw new InvalidOperationException("Cannot set image outside of a transaction");
        var command = new SetJpegCommand(
            transactionId: transaction.Id,
            path: RecipeImagePath(id),
            stream: stream);
        await transaction.ExecuteAsync(command, cancellationToken);
    }

    private string RecipeImagePath(string id) => Path.Combine(options.Value.RootDirectory, id, "image.jpg");
}
