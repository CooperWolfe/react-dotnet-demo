using Backend.Core.Repositories;
using Backend.Core.Transactions;
using Backend.FileSystem.Configuration;
using Backend.FileSystem.Transactions;
using Microsoft.Extensions.Options;

namespace Backend.FileSystem.Repositories;
internal class FileSystemRecipeImageRepositoryFactory : IRepositoryFactory<IRecipeImageRepository>
{
    private readonly IOptions<FileSystemConfiguration> options;

    public FileSystemRecipeImageRepositoryFactory(IOptions<FileSystemConfiguration> options)
    {
        this.options = options;
    }

    public IRecipeImageRepository Create(ITransaction transaction)
    {
        if (transaction is not FileSystemTransaction fsTransaction)
        {
            throw new InvalidOperationException("Cannot use a non-FS transaction for an FS repository");
        }
        return new FileSystemRecipeImageRepository(fsTransaction, options);
    }
}
