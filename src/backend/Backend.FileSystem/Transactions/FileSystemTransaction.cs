using Backend.Core.Transactions;
using Backend.FileSystem.Commands;
using Microsoft.Extensions.Logging;

namespace Backend.FileSystem.Transactions;
internal class FileSystemTransaction : IImageTransaction
{
    private readonly ILogger<FileSystemTransaction> logger;

    public FileSystemTransaction(ILogger<FileSystemTransaction> logger)
    {
        Id = Guid.NewGuid().ToString();
        this.logger = logger;
    }

    public string Id { get; }
    public ICommand? Command { get; private set; }

    public async Task ExecuteAsync(ICommand command, CancellationToken cancellationToken)
    {
        if (Command is not null)
        {
            throw new InvalidOperationException("Can currently only execute one command in a transaction");
        }
        Command = command;
        await Command.PrepareAsync(cancellationToken);
    }

    public async Task CommitAsync(CancellationToken cancellationToken = default)
    {
        if (Command is null) return;
        await Command.ExecuteAsync(cancellationToken);
    }
    public void Dispose()
    {
        if (Command is null) return;
        try
        {
            Command.Dispose();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, $"Failed to dispose command '{Command.GetType().FullName}' with transaction ID '{Id}'");
        }
    }
    public ValueTask DisposeAsync()
    {
        Dispose();
        return ValueTask.CompletedTask;
    }
    public Task RollbackAsync() => Command?.RollbackAsync() ?? Task.CompletedTask;
}