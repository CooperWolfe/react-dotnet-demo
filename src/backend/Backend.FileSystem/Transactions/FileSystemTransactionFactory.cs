using Backend.Core.Transactions;
using Microsoft.Extensions.Logging;

namespace Backend.FileSystem.Transactions;
internal class FileSystemTransactionFactory : ITransactionFactory<IImageTransaction>
{
    private readonly ILoggerFactory loggerFactory;

    public FileSystemTransactionFactory(ILoggerFactory loggerFactory)
    {
        this.loggerFactory = loggerFactory;
    }

    public IImageTransaction Create()
    {
        return new FileSystemTransaction(loggerFactory.CreateLogger<FileSystemTransaction>());
    }
}
