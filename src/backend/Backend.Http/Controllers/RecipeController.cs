using Backend.Core;
using Backend.Core.Commands;
using Backend.Http.Dtos;
using Backend.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Backend.Http.Controllers;
[ApiController]
[Route("[controller]")]
public class RecipeController : ControllerBase
{
    private readonly ILogger<RecipeController> logger;
    private readonly IBackendService backendService;
    private readonly IStringLocalizer<Resources> localizer;

    public RecipeController(
        ILogger<RecipeController> logger,
        IBackendService backendService,
        IStringLocalizer<Resources> localizer)
    {
        this.logger = logger;
        this.backendService = backendService;
        this.localizer = localizer;
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreateRecipeRequestDto requestBody, CancellationToken cancellationToken)
    {
        var command = requestBody.ToCommand();
        var response = await backendService.CreateRecipe(command, cancellationToken);
        var responseBody = new CreateRecipeResponseDto(createdRecipeId: response.CreatedRecipeId);
        return CreatedAtAction(nameof(GetOne), new { id = response.CreatedRecipeId }, responseBody);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Update([FromRoute] string id, [FromBody] UpdateRecipeRequestDto body, CancellationToken cancellationToken)
    {
        var command = body.ToCommand(id: id);
        await backendService.UpdateRecipe(command, cancellationToken);
        return NoContent();
    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(GetOneRecipeResponseDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetOne([FromRoute] string id, CancellationToken cancellationToken)
    {
        var command = new GetOneRecipeCommand(id: id);
        var response = await backendService.GetOneRecipe(command, cancellationToken);
        if (response.Recipe is null)
        {
            return NotFound(localizer[Resources.NoSuchRecipePrompt, id].Value);
        }
        var responseBody = new GetOneRecipeResponseDto(response.Recipe);
        return Ok(responseBody);
    }

    [HttpGet]
    [ProducesResponseType(typeof(SearchRecipesResponseDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Search(
        [FromQuery] string? query,
        [FromQuery] uint? offset,
        [FromQuery] uint? limit,
        [FromQuery] DateTime? until,
        CancellationToken cancellationToken)
    {
        var command = new SearchRecipesCommand(
            query: query,
            offset: offset,
            limit: limit,
            until: until);
        var response = await backendService.SearchRecipes(command, cancellationToken);
        var responseBody = new SearchRecipesResponseDto(response.Recipes);
        return Ok(responseBody);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete([FromRoute] string id, CancellationToken cancellationToken)
    {
        var command = new DeleteRecipeCommand(id: id);
        await backendService.DeleteRecipe(command, cancellationToken);
        return NoContent();
    }

    [HttpGet("{id}/image")]
    public async Task<IActionResult> GetImage([FromRoute] string id, CancellationToken cancellationToken)
    {
        var command = new GetRecipeImageCommand(id);
        var response = await backendService.GetRecipeImage(command, cancellationToken);
        return File(response.Image, contentType: response.Type.ToContentType());
    }

    [HttpPost("{id}/image")]
    public async Task<IActionResult> SetImage([FromRoute] string id, [FromForm] IFormFile image, CancellationToken cancellationToken)
    {
        var command = new SetRecipeImageCommand(
            recipeId: id,
            imageType: ImageTypeExtensions.FromContentType(image.ContentType),
            stream: image.OpenReadStream());
        await backendService.SetRecipeImage(command, cancellationToken);
        return CreatedAtAction(nameof(GetImage), new { id = id }, null);
    }

    [HttpDelete("{id}/image")]
    public async Task<IActionResult> DeleteImage([FromRoute] string id, CancellationToken cancellationToken)
    {
        var command = new DeleteRecipeImageCommand(id: id);
        await backendService.DeleteRecipeImage(command, cancellationToken);
        return NoContent();
    }
}