using Backend.Core.Commands;

namespace Backend.Http.Dtos;
public class CreateIngredientRequestDto
{
    public string Name { get; set; } = "";
    public float AmountValue { get; set; } = 0f;
    public string? AmountUnit { get; set; } = null;

    internal CreateIngredientCommand ToCommand()
    {
        return new CreateIngredientCommand(name: Name, amountValue: AmountValue, amountUnit: AmountUnit);
    }
}