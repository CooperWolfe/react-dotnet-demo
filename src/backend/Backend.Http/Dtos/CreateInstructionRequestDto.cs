using Backend.Core.Commands;

namespace Backend.Http.Dtos;
public class CreateInstructionRequestDto
{
    public string Description { get; set; } = "";

    internal CreateInstructionCommand ToCommand()
    {
        return new CreateInstructionCommand(description: Description);
    }
}