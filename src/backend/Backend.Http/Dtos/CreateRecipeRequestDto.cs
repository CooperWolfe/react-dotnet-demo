using Backend.Core.Commands;

namespace Backend.Http.Dtos;
public class CreateRecipeRequestDto
{
    public string Name { get; set; } = "";
    public string Description { get; set; } = "";
    public IEnumerable<CreateIngredientRequestDto> Ingredients { get; set; } = new CreateIngredientRequestDto[0];
    public IEnumerable<CreateInstructionRequestDto> Instructions { get; set; } = new CreateInstructionRequestDto[0];

    internal CreateRecipeCommand ToCommand()
    {
        return new CreateRecipeCommand(
            name: Name,
            description: Description,
            ingredients: Ingredients.Select(i => i.ToCommand()),
            instructions: Instructions.Select(i => i.ToCommand()));
    }
}