namespace Backend.Http.Dtos;
public class CreateRecipeResponseDto
{
    internal CreateRecipeResponseDto(string createdRecipeId)
    {
        CreatedRecipeId = createdRecipeId;
    }

    public string CreatedRecipeId { get; }
}