using Backend.Core.Model.Read;

namespace Backend.Http.Dtos;
public class GetOneRecipeResponseDto
{
    public GetOneRecipeResponseDto(IRecipe recipe)
    {
        Name = recipe.Name;
        Description = recipe.Description;
        Ingredients = recipe.Ingredients.Select(i => new IngredientDto(i));
        Instructions = recipe.Instructions.Select(i => new InstructionDto(i));
    }

    public string Name { get; }
    public string Description { get; }
    public IEnumerable<IngredientDto> Ingredients { get; }
    public IEnumerable<InstructionDto> Instructions { get; }

    public class IngredientDto
    {
        public IngredientDto(IIngredient ingredient)
        {
            Name = ingredient.Name;
            AmountValue = ingredient.AmountValue;
            AmountUnit = ingredient.AmountUnit;
        }

        public string Name { get; }
        public float AmountValue { get; }
        public string? AmountUnit { get; }
    }
    public class InstructionDto
    {
        public InstructionDto(IInstruction instruction)
        {
            Description = instruction.Description;
        }
        
        public string Description { get; }
    }
}