using Backend.Core.Model.Read;

namespace Backend.Http.Dtos;
public class SearchRecipesResponseDto
{
    public SearchRecipesResponseDto(IEnumerable<IRecipe> recipes)
    {
        Recipes = recipes.Select(r => new RecipeDto(r));
    }

    public IEnumerable<RecipeDto> Recipes { get; }

    public class RecipeDto
    {
        public RecipeDto(IRecipe recipe)
        {
            Id = recipe.Id;
            Name = recipe.Name;
            Description = recipe.Description;
        }

        public string Id { get; set; }
        public string Name { get; }
        public string Description { get; }
    }
}