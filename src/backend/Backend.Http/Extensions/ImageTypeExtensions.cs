using Backend.Core.Model;

namespace Backend.Http.Extensions;
internal static class ImageTypeExtensions
{
    private const string JPEG = @"image/jpeg";
    private const string PNG = @"image/png";

    public static ImageType FromContentType(string contentType)
    {
        return contentType switch
        {
            JPEG => ImageType.JPEG,
            PNG => ImageType.PNG,
            _ => (ImageType)(-1)
        };
    }
    public static string ToContentType(this ImageType imageType)
    {
        return imageType switch
        {
            ImageType.JPEG => JPEG,
            ImageType.PNG => PNG,
            _ => throw new ArgumentException("Invalid image type", nameof(imageType))
        };
    }
}