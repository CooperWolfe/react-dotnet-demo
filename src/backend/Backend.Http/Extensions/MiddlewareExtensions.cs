using System.Net.Mime;
using Backend.Core.Exceptions;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Backend.Http.Extensions;
internal static class MiddlewareExtensions
{
    public static void UseCustomExceptionHandling(this IApplicationBuilder app)
    {
        app.UseExceptionHandler(app =>
        {
            app.Run(async ctx =>
            {
                var exceptionHandlerPathFeature = ctx.Features.Get<IExceptionHandlerPathFeature>();
                switch (exceptionHandlerPathFeature?.Error)
                {
                    case NotFoundException ex:
                        {
                            ctx.Response.StatusCode = StatusCodes.Status404NotFound;
                            ctx.Response.ContentType = MediaTypeNames.Text.Plain;
                            await ctx.Response.WriteAsync(ex.Message);
                            break;
                        }
                    case InvalidCommandException ex:
                        {
                            ctx.Response.StatusCode = StatusCodes.Status400BadRequest;
                            ctx.Response.ContentType = MediaTypeNames.Application.Json;
                            ModelStateDictionary modelState = new();
                            ex.ValidationResult.AddToModelState(modelState);
                            var serializableError = new SerializableError(modelState);
                            await ctx.Response.WriteAsJsonAsync(serializableError);
                            break;
                        }
                }
            });
        });
    }
}