using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace Backend.Http;
public static class HostBuilderFactory
{
    public static IHostBuilder Create(string[] args, Action<IServiceCollection> configureDelegate) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureServices(configureDelegate)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
                webBuilder.UseKestrel(opts =>
                {
                    var config = opts.ApplicationServices.GetRequiredService<IConfiguration>();
                    opts.ListenAnyIP(int.Parse(config["HTTP_PORT"]!), lo =>
                    {
                        lo.Protocols = HttpProtocols.Http1;
                    });
                });
            });
}