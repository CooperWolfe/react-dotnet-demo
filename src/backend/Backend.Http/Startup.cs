using Backend.Http.Extensions;

namespace Backend.Http;
class Startup
{
    private readonly IConfiguration configuration;

    public Startup(IConfiguration configuration)
    {
        this.configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        services.AddCors(opt => opt.AddDefaultPolicy(opt => opt.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin()));
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseCustomExceptionHandling();
        app.UseRequestLocalization();
        if (env.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        app.UseRouting();
        app.UseHttpsRedirection();
        app.UseCors();
        app.UseEndpoints(endpoints => endpoints.MapControllers());
    }
}