using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var configuration = new ConfigurationBuilder()
    .AddEnvironmentVariables()
    .Build();
void ConfigureDelegate(IServiceCollection services)
{
    services.AddCore(opt => opt.Configure(cfg =>
    {
        cfg.DefaultRecipeSearchLimit = uint.Parse(configuration["BACKEND_DEFAULT_RECIPE_SEARCH_LIMIT"]!);
    }));
    services.AddSqlServer(opt => opt.Configure(cfg =>
    {
        cfg.User = configuration["SQL_USER"]!;
        cfg.Password = configuration["SQL_PASSWORD"]!;
        cfg.Host = configuration["SQL_HOST"]!;
        cfg.Port = int.Parse(configuration["SQL_PORT"]!);
        cfg.Database = configuration["SQL_DATABASE"]!;
    }));
    services.AddFileSystem(opt => opt.Configure(cfg =>
    {
        cfg.RootDirectory = configuration["IMAGE_ROOT_DIRECTORY"]!;
    }));
}

var httpHost = Backend.Http.HostBuilderFactory.Create(args, ConfigureDelegate).Build();
await httpHost.RunAsync();