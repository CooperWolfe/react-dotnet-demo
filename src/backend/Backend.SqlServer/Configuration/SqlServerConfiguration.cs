namespace Backend.SqlServer.Configuration;
public class SqlServerConfiguration
{
    public string Host { get; set; } = "";
    public int Port { get; set; } = 1433;
    public string User { get; set; } = "";
    public string Password { get; set; } = "";
    public string Database { get; set; } = "";

    public string ConnectionString => $"User ID={User};Password={Password};Server={Host},{Port};Database={Database};TrustServerCertificate=True";
}