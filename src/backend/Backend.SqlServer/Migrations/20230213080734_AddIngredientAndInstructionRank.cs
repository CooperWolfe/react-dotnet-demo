﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Backend.SqlServer.Migrations
{
    /// <inheritdoc />
    public partial class AddIngredientAndInstructionRank : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Rank",
                table: "Instructions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Rank",
                table: "Ingredients",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rank",
                table: "Instructions");

            migrationBuilder.DropColumn(
                name: "Rank",
                table: "Ingredients");
        }
    }
}
