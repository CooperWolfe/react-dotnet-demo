using Backend.SqlServer.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Backend.SqlServer.Model;
internal class BackendContext : DbContext
{
    private readonly IOptions<SqlServerConfiguration> options;

    public BackendContext(IOptions<SqlServerConfiguration> options)
    {
        this.options = options;
    }

    public DbSet<Recipe>? Recipes { get; set; }
    public DbSet<Ingredient>? Ingredients { get; set; }
    public DbSet<Instruction>? Instructions { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder dcob)
    {
        dcob.UseSqlServer(options.Value.ConnectionString);
    }
}