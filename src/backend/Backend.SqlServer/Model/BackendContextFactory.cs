using Backend.SqlServer.Configuration;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Backend.SqlServer.Model;
internal class BackendContextFactory : IDesignTimeDbContextFactory<BackendContext>
{
    public BackendContext CreateDbContext(string[] args)
    {
        return new BackendContext(new EnvironmentVariableOptions());
    }

    private class EnvironmentVariableOptions : IOptions<SqlServerConfiguration>
    {
        public EnvironmentVariableOptions()
        {
            var env = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .Build();
            Value = new()
            {
                Host = env["SQL_HOST"]!,
                Port = int.Parse(env["SQL_PORT"]!),
                User = env["SQL_USER"]!,
                Password = env["SQL_PASSWORD"]!,
                Database = env["SQL_DATABASE"]!,
            };
        }

        public SqlServerConfiguration Value { get; }
    }
}
