using System.ComponentModel.DataAnnotations.Schema;
using Backend.Core.Model.Read;

namespace Backend.SqlServer.Model;
internal class Ingredient : IIngredient
{
    public Ingredient()
    {
        Id = "";
        Name = "";
    }
    public Ingredient(Core.Model.Ingredient ingredient, int rank)
    {
        Id = Guid.NewGuid().ToString();
        Name = ingredient.Name;
        AmountValue = ingredient.AmountValue;
        AmountUnit = ingredient.AmountUnit;
        Rank = rank;
    }

    [Column(TypeName = "char(36)")]
    public string Id { get; set; }

    [Column(TypeName = "nvarchar(max)")]
    public string Name { get; set; }

    [Column(TypeName = "decimal(6, 2)")]
    public float AmountValue { get; set; }

    [Column(TypeName = "nvarchar(5)")]
    public string? AmountUnit { get; set; }

    public int Rank { get; set; }

    public Recipe? Recipe { get; set; }
}