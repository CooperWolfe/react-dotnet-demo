using System.ComponentModel.DataAnnotations.Schema;
using Backend.Core.Model.Read;

namespace Backend.SqlServer.Model;
internal class Instruction : IInstruction
{
    public Instruction()
    {
        Id = "";
        Description = "";
    }
    public Instruction(Core.Model.Instruction instruction, int rank)
    {
        Id = Guid.NewGuid().ToString();
        Description = instruction.Description;
        Rank = rank;
    }

    [Column(TypeName = "char(36)")]
    public string Id { get; set; }

    [Column(TypeName = "nvarchar(max)")]
    public string Description { get; set; }

    public int Rank { get; set; }

    public Recipe? Recipe { get; set; }
}