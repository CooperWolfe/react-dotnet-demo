using System.ComponentModel.DataAnnotations.Schema;
using Backend.Core.Model.Read;
using Microsoft.EntityFrameworkCore;

namespace Backend.SqlServer.Model;
[Index(nameof(NameLatinBin), nameof(CreatedAt))]
internal class Recipe : IRecipe
{
    public Recipe()
    {
        Id = "";
        Name = "";
        Description = "";
        NameLatinBin = "";
        Ingredients = new();
        Instructions = new();
    }
    public Recipe(Core.Model.Recipe recipe)
    {
        Id = recipe.Id;
        Name = recipe.Name;
        Description = recipe.Description;
        NameLatinBin = recipe.Name;
        Ingredients = recipe.Ingredients.Select((i, r) => new Ingredient(i, r)).ToList();
        Instructions = recipe.Instructions.Select((i, r) => new Instruction(i, r)).ToList();
        CreatedAt = recipe.CreatedAt;
    }

    [Column(TypeName = "char(36)")]
    public string Id { get; set; }

    [Column(TypeName = "nvarchar(50)")]
    public string Name { get; set; }

    [Column(TypeName = "nvarchar(max)")]
    public string Description { get; set; }

    [Column(TypeName = "nvarchar(50)")]
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public string NameLatinBin { get; set; }
    
    public DateTime CreatedAt { get; set; }

    public List<Ingredient> Ingredients { get; }
    public List<Instruction> Instructions { get; }

    IEnumerable<IIngredient> IRecipe.Ingredients => Ingredients;
    IEnumerable<IInstruction> IRecipe.Instructions => Instructions;
}