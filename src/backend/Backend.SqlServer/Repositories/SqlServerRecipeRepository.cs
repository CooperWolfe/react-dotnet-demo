using Backend.Core.Model.Read;
using Backend.Core.Repositories;
using Backend.SqlServer.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Backend.SqlServer.Repositories;
internal class SqlServerRecipeRepository : IRecipeRepository
{
    private readonly BackendContext context;
    private readonly IStringLocalizer<Resources> localizer;

    public SqlServerRecipeRepository(
        BackendContext context,
        IStringLocalizer<Resources> localizer)
    {
        this.context = context;
        this.localizer = localizer;
    }

    public Task Create(Core.Model.Recipe coreRecipe, CancellationToken cancellationToken)
    {
        var recipe = new Recipe(coreRecipe);
        context.Recipes!.Add(recipe);
        return Task.CompletedTask;
    }

    public Task Delete(string id, CancellationToken cancellationToken)
    {
        context.Ingredients!.RemoveRange(context.Ingredients!.Where(i => i.Recipe!.Id == id));
        context.Instructions!.RemoveRange(context.Instructions!.Where(i => i.Recipe!.Id == id));
        context.Recipes!.RemoveRange(context.Recipes.Where(r => r.Id == id));
        return Task.CompletedTask;
    }

    public async Task<IRecipe?> ReadById(string id, CancellationToken cancellationToken)
    {
        var recipe = await context.Recipes!
            .Include(r => r.Ingredients.OrderBy(i => i.Rank))
            .Include(r => r.Instructions.OrderBy(i => i.Rank))
            .AsNoTracking()
            .SingleOrDefaultAsync(r => r.Id == id, cancellationToken);
        return recipe;
    }

    public Task<IEnumerable<IRecipe>> Search(string? query, DateTime until, uint offset, uint limit, CancellationToken cancellationToken)
    {
        var recipes = query == null ? context.Recipes! : localizer[Resources.Alphabet].Value switch
        {
            Resources.Alphabets.Latin => context.Recipes!.Where(rec => EF.Functions.Like(rec.NameLatinBin, EF.Functions.Collate($"%{query.ToUpper()}%", Resources.Collations.Latin))),
            _ => context.Recipes!.Where(rec => EF.Functions.Like(rec.NameLatinBin, EF.Functions.Collate($"%{query.ToUpper()}%", Resources.Collations.Latin)))
        };
        recipes = recipes
            .Where(rec => rec.CreatedAt <= until)
            .OrderByDescending(rec => rec.CreatedAt)
            .Skip(Convert.ToInt32(offset))
            .Take(Convert.ToInt32(limit));
        return Task.FromResult<IEnumerable<IRecipe>>(recipes);
    }

    public Task Update(Core.Model.Recipe coreRecipe, CancellationToken cancellationToken)
    {
        var recipe = new Recipe(coreRecipe);
        context.Recipes!.Attach(recipe);
        context.Ingredients!.RemoveRange(context.Ingredients.Where(i => i.Recipe!.Id == recipe.Id));
        context.Instructions!.RemoveRange(context.Instructions.Where(i => i.Recipe!.Id == recipe.Id));
        context.SaveChanges();
        context.Ingredients!.AddRange(recipe.Ingredients);
        context.Instructions!.AddRange(recipe.Instructions);
        context.Recipes!.Update(recipe);
        return Task.CompletedTask;
    }
}
