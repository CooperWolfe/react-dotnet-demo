using Backend.Core.Repositories;
using Backend.Core.Transactions;
using Backend.SqlServer.Transactions;
using Microsoft.Extensions.Localization;

namespace Backend.SqlServer.Repositories;
internal class SqlServerRecipeRepositoryFactory : IRepositoryFactory<IRecipeRepository>
{
    private readonly IStringLocalizer<Resources> localizer;

    public SqlServerRecipeRepositoryFactory(IStringLocalizer<Resources> localizer)
    {
        this.localizer = localizer;
    }

    public IRecipeRepository Create(ITransaction transaction)
    {
        if (transaction is not SqlServerTransaction sqlServerTransaction)
        {
            throw new ArgumentException("SQL server transactions must be used with a SQL server repository.", nameof(transaction));
        }

        return new SqlServerRecipeRepository(
            context: sqlServerTransaction.Context,
            localizer: localizer);
    }
}
