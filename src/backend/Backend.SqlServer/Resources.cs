namespace Backend.SqlServer;
public class Resources
{
    public const string Alphabet = nameof(Alphabet);

    public static class Alphabets
    {
        public const string Latin = nameof(Latin);
    }
    public static class Collations
    {
        public const string Latin = @"Latin1_General_100_Bin2";
    }
}