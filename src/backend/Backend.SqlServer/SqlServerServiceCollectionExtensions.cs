using Microsoft.Extensions.Options;
using Backend.Core.Transactions;
using Backend.Core.Repositories;
using Backend.SqlServer.Transactions;
using Backend.SqlServer.Configuration;
using Backend.SqlServer.Model;
using Backend.SqlServer.Repositories;

namespace Microsoft.Extensions.DependencyInjection;
public static class SqlServerServiceCollectionExtensions
{
    public static void AddSqlServer(this IServiceCollection services, Action<OptionsBuilder<SqlServerConfiguration>> configure)
    {
        services.AddLocalization();
        var sqlServerConfigBuilder = services.AddOptions<SqlServerConfiguration>();
        configure(sqlServerConfigBuilder);

        services.AddDbContext<BackendContext>();

        services.AddRepository<IRecipeRepository, SqlServerRecipeRepository, SqlServerRecipeRepositoryFactory>();

        services.AddTransient<ITransactionFactory<IBackendTransaction>, SqlServerTransactionFactory>();
    }

    private static void AddRepository<TIRepository, TRepository, TRepositoryFactory>(this IServiceCollection services)
        where TIRepository : class
        where TRepository : class, TIRepository
        where TRepositoryFactory : class, IRepositoryFactory<TIRepository>
    {
        services.AddTransient<IRepositoryFactory<TIRepository>, TRepositoryFactory>();
        services.AddScoped<TIRepository, TRepository>();
    }
}