using Backend.Core.Transactions;
using Backend.SqlServer.Model;
using Microsoft.EntityFrameworkCore.Storage;

namespace Backend.SqlServer.Transactions;
internal class SqlServerTransaction : IBackendTransaction
{
    private readonly IDbContextTransaction innerTransaction;

    public SqlServerTransaction(BackendContext context)
    {
        innerTransaction = context.Database.BeginTransaction();
        Context = context;
    }

    public BackendContext Context { get; }

    public async Task CommitAsync(CancellationToken cancellationToken = default)
    {
        await Context.SaveChangesAsync(cancellationToken);
        await innerTransaction.CommitAsync(cancellationToken);
    }
    public void Dispose() => innerTransaction.Dispose();
    public ValueTask DisposeAsync() => innerTransaction.DisposeAsync();
    public Task RollbackAsync() => innerTransaction.RollbackAsync();
}
