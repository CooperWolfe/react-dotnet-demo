using Backend.Core.Transactions;
using Backend.SqlServer.Model;

namespace Backend.SqlServer.Transactions;
internal class SqlServerTransactionFactory : ITransactionFactory<IBackendTransaction>
{
    private readonly BackendContext context;

    public SqlServerTransactionFactory(BackendContext context)
    {
        this.context = context;
    }

    public IBackendTransaction Create()
    {
        return new SqlServerTransaction(context);
    }
}
