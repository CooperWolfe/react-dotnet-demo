using Backend.Core.Commands;
using Backend.Core.CommandHandlers;
using Xunit;
using Backend.UnitTests.Fakes;
using Backend.Core;
using Backend.Core.Exceptions;
using Moq;
using Backend.Core.Model.Read;
using Backend.Core.Repositories;

namespace Backend.UnitTests.CommandHandlers;
public class GetRecipeImageCommandHandlerTests
{
    [Fact]
    public async Task Handle_NoSuchRecipe_ThrowsNotFoundException()
    {
        var command = CreateCommand();
        var stubRecipeRepository = new FakeRecipeRepository();
        stubRecipeRepository.SetupReadById().ReturnsAsync((IRecipe?)null);
        var commandHandler = CreateCommandHandler(
            recipeRepository: stubRecipeRepository);
    
        Func<Task> act = () => commandHandler.Handle(command, default);

        await Assert.ThrowsAsync<NotFoundException>(act);
    }

    [Fact]
    public async Task Handle_RecipeHasNoImage_UsesDefaultImage()
    {
        var command = CreateCommand();
        var mockImageRepository = new FakeImageRepository();
        mockImageRepository.SetupGetImage().ReturnsAsync((IRecipeImage?)null);
        var commandHandler = CreateCommandHandler(recipeImageRepository: mockImageRepository);
    
        await commandHandler.Handle(command, default);

        mockImageRepository.VerifyGetDefaultImage();
    }

    private static GetRecipeImageCommand CreateCommand()
    {
        return new GetRecipeImageCommand(
            id: "abc123");
    }

    private static GetRecipeImageCommandHandler CreateCommandHandler(
        IRecipeRepository? recipeRepository = null,
        IRecipeImageRepository? recipeImageRepository = null)
    {
        return new GetRecipeImageCommandHandler(
            logger: new FakeLogger<GetRecipeImageCommandHandler>(),
            recipeImageRepository: recipeImageRepository ?? new FakeImageRepository(),
            recipeRepository: recipeRepository ?? new FakeRecipeRepository(),
            localizer: new FakeStringLocalizer<Resources>());
    }
}