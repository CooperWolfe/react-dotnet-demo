using Backend.Core.Commands;
using Backend.Core.CommandHandlers;
using Xunit;
using Backend.UnitTests.Fakes;
using Backend.Core.Repositories;
using Backend.Core.Configuration;
using System.Collections;
using Backend.Core.Utilities;

namespace Backend.UnitTests.CommandHandlers;
public class SearchRecipesCommandHandlerTests
{
    [Theory]
    [InlineData(null)] // Null
    [InlineData("")] // Empty
    [InlineData("\n\r\t ")] // Whitespace
    public async Task Handle_NoQuery_UsesNullQueryForSearch(string? query)
    {
        var command = CreateCommand(query: query);
        var mockRecipeRepository = new FakeRecipeRepository();
        var commandHandler = CreateCommandHandler(recipeRepository: mockRecipeRepository);
    
        await commandHandler.Handle(command, default);

        mockRecipeRepository.VerifySearch(query: q => q == null);
    }

    [Fact]
    public async Task Handle_NoOffset_Uses0OffsetForSearch()
    {
        var command = CreateCommand(offset: null);
        var mockRecipeRepository = new FakeRecipeRepository();
        var commandHandler = CreateCommandHandler(recipeRepository: mockRecipeRepository);
    
        await commandHandler.Handle(command, default);

        mockRecipeRepository.VerifySearch(offset: o => o == 0u);
    }

    [Theory]
    [InlineData(0u)]
    [InlineData(10u)]
    [InlineData(100u)]
    public async Task Handle_NoLimit_UsesConfiguredLimitForSearch(uint configuredLimit)
    {
        var command = CreateCommand(limit: null);
        var stubConfiguration = new BackendConfiguration
        {
            DefaultRecipeSearchLimit = configuredLimit
        };
        var mockRecipeRepository = new FakeRecipeRepository();
        var commandHandler = CreateCommandHandler(
            configuration: stubConfiguration,
            recipeRepository: mockRecipeRepository);
    
        await commandHandler.Handle(command, default);

        mockRecipeRepository.VerifySearch(limit: l => l == configuredLimit);
    }

    private class RequestContextTimestamp : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new DateTime(2018, 1, 1)
            };
            yield return new object[]
            {
                new DateTime(2023, 2, 9)
            };
            yield return new object[]
            {
                new DateTime(3000, 12, 31)
            };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    [Theory]
    [ClassData(typeof(RequestContextTimestamp))]
    public async Task Handle_NoUntil_UsesRequestContextTimestampForSearch(DateTime requestContextTimestamp)
    {
        var command = CreateCommand(until: null);
        var stubRequestContext = new FakeRequestContext(timestamp: requestContextTimestamp);
        var mockRecipeRepository = new FakeRecipeRepository();
        var commandHandler = CreateCommandHandler(
            requestContext: stubRequestContext,
            recipeRepository: mockRecipeRepository);
    
        await commandHandler.Handle(command, default);

        mockRecipeRepository.VerifySearch(until: u => u == requestContextTimestamp);
    }

    private static SearchRecipesCommand CreateCommand(
        string? query = "abc123",
        uint? offset = 10u,
        uint? limit = 10u,
        DateTime? until = null)
    {
        return new SearchRecipesCommand(
            query: query,
            offset: offset,
            limit: limit,
            until: until);
    }

    private static SearchRecipesCommandHandler CreateCommandHandler(
        IRecipeRepository? recipeRepository = null,
        BackendConfiguration? configuration = null,
        IRequestContext? requestContext = null)
    {
        return new SearchRecipesCommandHandler(
            logger: new FakeLogger<SearchRecipesCommandHandler>(),
            recipeRepository: recipeRepository ?? new FakeRecipeRepository(),
            requestContext: requestContext ?? new FakeRequestContext(timestamp: new DateTime(2023, 2, 9)),
            options: new FakeOptions<BackendConfiguration>(configuration));
    }
}