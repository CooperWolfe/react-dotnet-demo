using Backend.Core.Commands;
using Backend.Core.CommandHandlers;
using Xunit;
using Backend.Core.Model;
using Backend.UnitTests.Fakes;
using Backend.Core;
using Backend.Core.Exceptions;
using Moq;
using Backend.Core.Model.Read;
using Backend.Core.Repositories;

namespace Backend.UnitTests.CommandHandlers;
public class SetRecipeImageCommandHandlerTests
{
    [Fact]
    public async Task Handle_NoSuchRecipe_ThrowsNotFoundException()
    {
        var command = CreateCommand();
        var stubRecipeRepository = new FakeRecipeRepository();
        stubRecipeRepository.SetupReadById().ReturnsAsync((IRecipe?)null);
        var commandHandler = CreateCommandHandler(recipeRepository: stubRecipeRepository);
    
        Func<Task> act = () => commandHandler.Handle(command, default);

        await Assert.ThrowsAsync<NotFoundException>(act);
    }

    private static SetRecipeImageCommand CreateCommand()
    {
        return new SetRecipeImageCommand(
            recipeId: "abc123",
            imageType: ImageType.JPEG,
            stream: new MemoryStream(new byte[] { 0x0 }));
    }

    private static SetRecipeImageCommandHandler CreateCommandHandler(
        IRecipeRepository? recipeRepository = null)
    {
        return new SetRecipeImageCommandHandler(
            logger: new FakeLogger<SetRecipeImageCommandHandler>(),
            recipeRepository: recipeRepository ?? new FakeRecipeRepository(),
            unitOfWork: new FakeImageUnitOfWork(),
            localizer: new FakeStringLocalizer<Resources>());
    }
}