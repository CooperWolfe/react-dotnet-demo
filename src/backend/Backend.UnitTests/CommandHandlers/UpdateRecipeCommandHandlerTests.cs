using Backend.Core.Commands;
using Backend.Core.CommandHandlers;
using Backend.UnitTests.Fakes;
using Xunit;
using Backend.Core.Exceptions;
using Moq;
using Backend.Core.Model.Read;
using Backend.Core.Repositories;
using Backend.Core;

namespace Backend.UnitTests.CommandHandlers;
public class UpdateRecipeCommandHandlerTests
{
    [Fact]
    public async Task Handle_NoSuchRecipe_ThrowsNotFoundException()
    {
        var command = CreateCommand();
        var stubRecipeRepository = new FakeRecipeRepository();
        stubRecipeRepository.SetupReadById().ReturnsAsync((IRecipe?)null);
        var commandHandler = CreateCommandHandler(recipeRepository: stubRecipeRepository);
    
        Func<Task> act = () => commandHandler.Handle(command, default);

        await Assert.ThrowsAsync<NotFoundException>(act);
    }

    private static UpdateRecipeCommand CreateCommand()
    {
        return new UpdateRecipeCommand(
            id: "abc123",
            name: "abc123",
            description: "abc123",
            ingredients: new CreateIngredientCommand[0],
            instructions: new CreateInstructionCommand[0]);
    }

    private static UpdateRecipeCommandHandler CreateCommandHandler(
        IRecipeRepository? recipeRepository = null)
    {
        return new UpdateRecipeCommandHandler(
            logger: new FakeLogger<UpdateRecipeCommandHandler>(),
            unitOfWork: new FakeBackendUnitOfWork(recipeRepository: recipeRepository),
            localizer: new FakeStringLocalizer<Resources>());
    }
}