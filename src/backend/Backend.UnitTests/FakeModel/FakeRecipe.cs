using Backend.Core.Model.Read;

namespace Backend.UnitTests.FakeModel;
internal class FakeRecipe : IRecipe
{
    public string Id { get; set; } = "abc123";
    public string Name { get; set; } = "abc123";
    public string Description { get; set; } = "abc123";
    public IEnumerable<IIngredient> Ingredients { get; set; } = new IIngredient[0];
    public IEnumerable<IInstruction> Instructions { get; set; } = new IInstruction[0];
    public DateTime CreatedAt { get; set; } = new DateTime(2023, 2, 8);
}
