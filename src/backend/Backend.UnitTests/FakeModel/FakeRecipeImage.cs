using Backend.Core.Model;
using Backend.Core.Model.Read;

namespace Backend.UnitTests.FakeModel;
class FakeRecipeImage : IRecipeImage
{
    public ImageType Type { get; set; } = ImageType.JPEG;
    public Stream Image { get; set; } = new MemoryStream(new byte[] { 0x0 });
}
