using Backend.Core.Repositories;
using Backend.Core.Transactions;

namespace Backend.UnitTests.Fakes;
class FakeBackendUnitOfWork : IBackendUnitOfWork
{
    public FakeBackendUnitOfWork(
        IRecipeRepository? recipeRepository = null)
    {
        RecipeRepository = recipeRepository ?? new FakeRecipeRepository();
    }

    public IRecipeRepository RecipeRepository { get; }

    public Task CommitAsync(CancellationToken cancellationToken = default) => Task.CompletedTask;
    public void Dispose() { }
    public ValueTask DisposeAsync() => ValueTask.CompletedTask;
    public Task RollbackAsync() => Task.CompletedTask;
}
