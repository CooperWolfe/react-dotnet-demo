using Backend.Core.Model;
using Backend.Core.Model.Read;
using Backend.Core.Repositories;
using Backend.UnitTests.FakeModel;
using Moq;

namespace Backend.UnitTests.Fakes;
class FakeImageRepository : Mock<IRecipeImageRepository>, IRecipeImageRepository
{
    public FakeImageRepository()
    {
        SetupGetImage().ReturnsAsync(new FakeRecipeImage());
        SetupGetDefaultImage().ReturnsAsync(new FakeRecipeImage());
    }

    internal Moq.Language.Flow.ISetup<IRecipeImageRepository, Task<IRecipeImage?>> SetupGetImage()
        => Setup(repo => repo.GetImage(It.IsAny<string>(), It.IsAny<CancellationToken>()));
    internal Moq.Language.Flow.ISetup<IRecipeImageRepository, Task<IRecipeImage>> SetupGetDefaultImage()
        => Setup(repo => repo.GetDefaultImage(It.IsAny<CancellationToken>()));

    internal void VerifyGetDefaultImage(Times? times = null)
        => Verify(repo => repo.GetDefaultImage(It.IsAny<CancellationToken>()), times ?? Times.AtLeastOnce());

    public Task<IRecipeImage> GetDefaultImage(CancellationToken cancellationToken)
        => Object.GetDefaultImage(cancellationToken);
    public Task<IRecipeImage?> GetImage(string id, CancellationToken cancellationToken)
        => Object.GetImage(id, cancellationToken);
    public Task SetRecipeImage(string id, ImageType type, Stream stream, CancellationToken cancellationToken)
        => Object.SetRecipeImage(id, type, stream, cancellationToken);
    public Task Delete(string id, CancellationToken cancellationToken)
        => Object.Delete(id, cancellationToken);
}
