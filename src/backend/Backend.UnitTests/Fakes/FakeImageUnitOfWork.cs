using Backend.Core.Repositories;
using Backend.Core.Transactions;

namespace Backend.UnitTests.Fakes;
class FakeImageUnitOfWork : IImageUnitOfWork
{
    public FakeImageUnitOfWork(IRecipeImageRepository? recipeImageRepository = null)
    {
        ImageRepository = recipeImageRepository ?? new FakeImageRepository();
    }

    public IRecipeImageRepository ImageRepository { get; }

    public Task CommitAsync(CancellationToken cancellationToken = default) => Task.CompletedTask;
    public void Dispose() { }
    public ValueTask DisposeAsync() => ValueTask.CompletedTask;
    public Task RollbackAsync() => Task.CompletedTask;
}
