using Microsoft.Extensions.Options;

namespace Backend.UnitTests.Fakes;
class FakeOptions<T> : IOptions<T> where T : class, new()
{
    public FakeOptions(T? value = null)
    {
        Value = value ?? new();
    }

    public T Value { get; }
}