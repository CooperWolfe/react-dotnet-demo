using System.Linq.Expressions;
using Backend.Core.Model;
using Backend.Core.Model.Read;
using Backend.Core.Repositories;
using Backend.UnitTests.FakeModel;
using Moq;

namespace Backend.UnitTests.Fakes;
class FakeRecipeRepository : Mock<IRecipeRepository>, IRecipeRepository
{
    public FakeRecipeRepository()
    {
        SetupReadById().ReturnsAsync(new FakeRecipe());
    }

    public Moq.Language.Flow.ISetup<IRecipeRepository, Task<IRecipe?>> SetupReadById()
        => Setup(repo => repo.ReadById(It.IsAny<string>(), It.IsAny<CancellationToken>()));

    public void VerifySearch(
        Times? times = null,
        Expression<Func<string?, bool>>? query = null,
        Expression<Func<DateTime, bool>>? until = null,
        Expression<Func<uint, bool>>? offset = null,
        Expression<Func<uint, bool>>? limit = null) => Verify(
            repo => repo.Search(
                It.Is<string?>(q => query == null ? true : query.Compile()(q)),
                It.Is<DateTime>(u => until == null ? true : until.Compile()(u)),
                It.Is<uint>(o => offset == null ? true : offset.Compile()(o)),
                It.Is<uint>(l => limit == null ? true : limit.Compile()(l)),
                It.IsAny<CancellationToken>()),
            times ?? Times.AtLeastOnce());

    public Task Create(Recipe recipe, CancellationToken cancellationToken)
        => Object.Create(recipe, cancellationToken);
    public Task<IRecipe?> ReadById(string id, CancellationToken cancellationToken)
        => Object.ReadById(id, cancellationToken);
    public Task Update(Recipe recipe, CancellationToken cancellationToken)
        => Object.Update(recipe, cancellationToken);
    public Task<IEnumerable<IRecipe>> Search(string? query, DateTime until, uint offset, uint limit, CancellationToken cancellationToken)
        => Object.Search(query, until, offset, limit, cancellationToken);
    public Task Delete(string id, CancellationToken cancellationToken)
        => Object.Delete(id, cancellationToken);
}
