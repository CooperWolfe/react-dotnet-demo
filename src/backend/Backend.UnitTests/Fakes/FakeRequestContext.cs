using Backend.Core.Utilities;

namespace Backend.UnitTests.Fakes;
class FakeRequestContext : IRequestContext
{
    public FakeRequestContext(DateTime timestamp)
    {
        Timestamp = timestamp;
    }

    public DateTime Timestamp { get; }
}