using Microsoft.Extensions.Localization;

namespace Backend.UnitTests.Fakes;
class FakeStringLocalizer : IStringLocalizer
{
    public LocalizedString this[string name] => new(name: name, value: "abc123");
    public LocalizedString this[string name, params object[] arguments] => new(name: name, value: "abc123");
    public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures) => new LocalizedString[0];
}
class FakeStringLocalizer<T> : IStringLocalizer<T>
{
    public LocalizedString this[string name] => new(name: name, value: "abc123");
    public LocalizedString this[string name, params object[] arguments] => new(name: name, value: "abc123");
    public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures) => new LocalizedString[0];
}
