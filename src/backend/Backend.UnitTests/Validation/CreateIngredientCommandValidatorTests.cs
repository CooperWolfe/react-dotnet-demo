using Backend.Core.Commands;
using Backend.Core.Validation;
using Backend.UnitTests.Fakes;
using Xunit;

namespace Backend.UnitTests.Validation;
public class CreateIngredientCommandValidatorTests
{
    private readonly CreateIngredientCommandValidator validator;

    public CreateIngredientCommandValidatorTests()
    {
        validator = new CreateIngredientCommandValidator(new FakeStringLocalizer());
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("")] // Empty string
    [InlineData("\n\r\t ")] // Whitespace
    public void Validate_InvalidName_IsInvalid(string invalidName)
    {
        var payload = CreatePayload(name: invalidName);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    [Theory]
    [InlineData(-1f)] // < 0
    [InlineData(0f)] // 0
    [InlineData(10000f)] // 10000
    [InlineData(10001f)] // > 10000
    public void Validate_InvalidAmountValue_IsInvalid(float invalidAmountValue)
    {
        var payload = CreatePayload(amountValue: invalidAmountValue);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    [Theory]
    [InlineData("abc123")] // > 5 characters
    public void Validate_InvalidAmountUnit_IsInvalid(string invalidAmountUnit)
    {
        var payload = CreatePayload(amountUnit: invalidAmountUnit);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static CreateIngredientCommand CreatePayload(
        string name = "abc123",
        float amountValue = 1f,
        string? amountUnit = null)
    {
        return new CreateIngredientCommand(
            name: name,
            amountValue: amountValue,
            amountUnit: amountUnit);
    }
}