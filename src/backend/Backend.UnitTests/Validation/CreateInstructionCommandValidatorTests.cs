using Backend.Core.Commands;
using Backend.Core.Validation;
using Xunit;

namespace Backend.UnitTests.Validation;
public class CreateInstructionCommandValidatorTests
{
    private readonly CreateInstructionCommandValidator validator;

    public CreateInstructionCommandValidatorTests()
    {
        validator = new CreateInstructionCommandValidator();
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("")] // Empty
    [InlineData("\n\r\t ")] // Whitespace
    public void Validate_InvalidDescription_IsInvalid(string invalidDescription)
    {
        var payload = CreatePayload(description: invalidDescription);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static CreateInstructionCommand CreatePayload(string description = "abc123")
    {
        return new CreateInstructionCommand(description: description);
    }
}