using Backend.Core.Commands;
using Backend.Core.Validation;
using Xunit;

namespace Backend.UnitTests.Validation;
public class DeleteRecipeImageCommandValidatorTests
{
    private readonly DeleteRecipeImageCommandValidator validator;

    public DeleteRecipeImageCommandValidatorTests()
    {
        validator = new DeleteRecipeImageCommandValidator();
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }
    
    [Theory]
    [InlineData("")] // empty
    [InlineData("\n\r\t ")] // whitespace
    public void Validate_InvalidId_IsInvalid(string invalidId)
    {
        var payload = CreatePayload(id: invalidId);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static DeleteRecipeImageCommand CreatePayload(string id = "abc123")
    {
        return new DeleteRecipeImageCommand(id: id);
    }
}