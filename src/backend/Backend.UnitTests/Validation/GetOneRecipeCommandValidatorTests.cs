using Backend.Core.Commands;
using Backend.Core.Validation;
using Xunit;

namespace Backend.UnitTests.Validation;
public class GetOneRecipeCommandValidatorTests
{
    private readonly GetOneRecipeCommandValidator validator;

    public GetOneRecipeCommandValidatorTests()
    {
        validator = new GetOneRecipeCommandValidator();
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("\n\r\t ")] // Whitespace
    [InlineData("")] // Empty
    public void Validate_InvalidId_IsInvalid(string invalidId)
    {
        var payload = CreatePayload(id: invalidId);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static GetOneRecipeCommand CreatePayload(string id = "abc123")
    {
        return new GetOneRecipeCommand(id: id);
    }
}