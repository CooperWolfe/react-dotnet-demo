using Backend.Core.Commands;
using Backend.Core.Validation;
using Xunit;

namespace Backend.UnitTests.Validation;
public class GetRecipeImageCommandValidatorTests
{
    private readonly GetRecipeImageCommandValidator validator;

    public GetRecipeImageCommandValidatorTests()
    {
        validator = new GetRecipeImageCommandValidator();
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("")] // empty
    [InlineData("\n\t\r ")] // whitespace
    public void Validate_InvalidId_IsInvalid(string invalidId)
    {
        var payload = CreatePayload(id: invalidId);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static GetRecipeImageCommand CreatePayload(string id = "abc123")
    {
        return new GetRecipeImageCommand(id: id);
    }
}