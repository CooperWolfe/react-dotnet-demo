using System.Collections;
using Backend.Core.Validation;
using Backend.UnitTests.Fakes;
using Xunit;

namespace Backend.UnitTests.Validation;
public class RecipeDescriptionValidatorTests
{
    private readonly RecipeDescriptionValidator validator;

    public RecipeDescriptionValidatorTests()
    {
        validator = new RecipeDescriptionValidator(new FakeStringLocalizer());
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = "abc123";

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    private class InvalidPayload : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] // contains non-space whitespace
            {
                "abc\n123"
            };
            var chars = new char[65536];
            Array.Fill(chars, 'a');
            yield return new object[] // length > 65535
            {
                new string(chars)
            };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    [Theory]
    [ClassData(typeof(InvalidPayload))]
    public void Validate_InvalidPayload_IsInvalid(string invalidPayload)
    {
        var validationResult = validator.Validate(invalidPayload);
    
        Assert.False(validationResult.IsValid);
    }
}