using Backend.Core.Validation;
using Backend.UnitTests.Fakes;
using Xunit;

namespace Backend.UnitTests.Validation;
public class RecipeNameValidatorTests
{
    private readonly RecipeNameValidator validator;

    public RecipeNameValidatorTests()
    {
        validator = new RecipeNameValidator(new FakeStringLocalizer());
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = "abc123";

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz")] // length > 50
    [InlineData("abc\n123")] // contains non-space whitespace
    public void Validate_InvalidRecipeName_IsInvalid(string invalidRecipeName)
    {
        var validationResult = validator.Validate(invalidRecipeName);
    
        Assert.False(validationResult.IsValid);
    }
}