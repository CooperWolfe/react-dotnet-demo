using System.Collections;
using Backend.Core;
using Backend.Core.Commands;
using Backend.Core.Utilities;
using Backend.Core.Validation;
using Backend.UnitTests.Fakes;
using Xunit;

namespace Backend.UnitTests.Validation;
public class SearchRecipesCommandValidatorTests
{
    private readonly SearchRecipesCommandValidator validator;

    public SearchRecipesCommandValidatorTests()
    {
        validator = new SearchRecipesCommandValidator(
            requestContext: new FakeRequestContext(new DateTime(2023, 2, 10)),
            localizer: new FakeStringLocalizer<Resources>());
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData(2147483648u)] // > int.MaxValue (2147483647)
    public void Validate_InvalidOffset_IsInvalid(uint? invalidOffset)
    {
        var payload = CreatePayload(offset: invalidOffset);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    [Theory]
    [InlineData(0u)] // zero
    [InlineData(1001u)] // > 1000
    public void Validate_InvalidLimit_IsInvalid(uint? invalidLimit)
    {
        var payload = CreatePayload(limit: invalidLimit);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private class InvalidUntil : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new DateTime(2023, 2, 11) // > Today
            };
            yield return new object[]
            {
                new DateTime(2023, 2, 10).LatestPossible().AddSeconds(1) // > Now (adjusted for skew and tz offset)
            };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    [Theory]
    [ClassData(typeof(InvalidUntil))]
    public void Validate_InvalidUntil_IsInvalid(DateTime invalidUntil)
    {
        var payload = CreatePayload(until: invalidUntil);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static SearchRecipesCommand CreatePayload(
        uint? offset = null,
        uint? limit = null,
        DateTime? until = null)
    {
        return new SearchRecipesCommand(
            query: null,
            offset: offset,
            limit: limit,
            until: until);
    }
}