using System.Collections;
using Backend.Core.Commands;
using Backend.Core.Model;
using Backend.Core.Validation;
using Xunit;

namespace Backend.UnitTests.Validation;
public class SetRecipeImageCommandValidatorTests
{
    private readonly SetRecipeImageCommandValidator validator;

    public SetRecipeImageCommandValidatorTests()
    {
        validator = new SetRecipeImageCommandValidator();
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("")] // Empty
    [InlineData("\n\r\t ")] // Whitespace
    public void Validate_InvalidRecipeId_IsInvalid(string invalidRecipeId)
    {
        var payload = CreatePayload(recipeId: invalidRecipeId);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    [Theory]
    [InlineData((ImageType)(-1))]
    public void Validate_InvalidImageType_IsInvalid(ImageType invalidImageType)
    {
        var payload = CreatePayload(imageType: invalidImageType);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private class InvalidStream : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new MemoryStream()
            };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    [Theory]
    [ClassData(typeof(InvalidStream))]
    public void Validate_InvalidStream_IsInvalid(Stream invalidStream)
    {
        var payload = CreatePayload(stream: invalidStream);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static SetRecipeImageCommand CreatePayload(
        string recipeId = "abc123",
        ImageType imageType = ImageType.JPEG,
        Stream? stream = null)
    {
        return new SetRecipeImageCommand(
            recipeId: recipeId,
            imageType: imageType,
            stream: stream ?? new MemoryStream(new byte[] { 0x0 }));
    }
}