using System.Collections;
using Backend.Core;
using Backend.Core.Commands;
using Backend.Core.Validation;
using Backend.UnitTests.Fakes;
using Xunit;

namespace Backend.UnitTests.Validation;
public class UpdateRecipeCommandValidatorTests
{
    private readonly UpdateRecipeCommandValidator validator;

    public UpdateRecipeCommandValidatorTests()
    {
        validator = new UpdateRecipeCommandValidator(new FakeStringLocalizer<Resources>());
    }

    [Fact]
    public void Validate_ValidPayload_IsValid()
    {
        var validPayload = CreatePayload();

        var validationResult = validator.Validate(validPayload);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("")] // empty
    [InlineData("\n\r\t ")] // whitespace
    public void Validate_InvalidId_IsInvalid(string invalidId)
    {
        var payload = CreatePayload(id: invalidId);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    [Theory]
    [InlineData("")] // empty
    [InlineData("\n\r\t ")] // whitespace
    [InlineData("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz")] // invalid
    public void Validate_InvalidName_IsInvalid(string invalidName)
    {
        var payload = CreatePayload(name: invalidName);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    [Theory]
    [InlineData("")] // empty
    [InlineData("\n\r\t ")] // whitespace
    [InlineData("abc\n123")] // invalid
    public void Validate_InvalidDescription_IsInvalid(string invalidDescription)
    {
        var payload = CreatePayload(description: invalidDescription);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private class InvalidIngredients : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new CreateIngredientCommand[0]
            };
            yield return new object[]
            {
                new[] {
                    new CreateIngredientCommand(
                        name: "",
                        amountValue: -1f,
                        amountUnit: "invalid")
                }
            };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    [Theory]
    [ClassData(typeof(InvalidIngredients))]
    public void Validate_InvalidIngredients_IsInvalid(IEnumerable<CreateIngredientCommand> invalidIngredients)
    {
        var payload = CreatePayload(ingredients: invalidIngredients);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private class InvalidInstructions : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new CreateInstructionCommand[0]
            };
            yield return new object[]
            {
                new[] {
                    new CreateInstructionCommand(description: "")
                }
            };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    [Theory]
    [ClassData(typeof(InvalidInstructions))]
    public void Validate_InvalidInstructions_IsInvalid(IEnumerable<CreateInstructionCommand> invalidInstructions)
    {
        var payload = CreatePayload(instructions: invalidInstructions);
    
        var validationResult = validator.Validate(payload);
    
        Assert.False(validationResult.IsValid);
    }

    private static UpdateRecipeCommand CreatePayload(
        string id = "abc123",
        string name = "abc123",
        string description = "abc123",
        IEnumerable<CreateIngredientCommand>? ingredients = null,
        IEnumerable<CreateInstructionCommand>? instructions = null)
    {
        return new UpdateRecipeCommand(
            id: id,
            name: name,
            description: description,
            ingredients: ingredients ?? new[] {
                new CreateIngredientCommand(
                    name: "abc123",
                    amountValue: 1f,
                    amountUnit: null)
            },
            instructions: instructions ?? new[] {
                new CreateInstructionCommand(description: "abc123")
            }
        );
    }
}