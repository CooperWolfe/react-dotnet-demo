using Backend.Core.Validation;
using FluentValidation;
using Xunit;

namespace Backend.UnitTests.Validation;
public class ValidationExtensionsTests
{
    [Theory]
    [InlineData("abc123")]
    [InlineData("abc 123")]
    public void DisallowNonSpaceWhitespace_ValidInput_IsValid(string validInput)
    {
        var validator = new FakeValidator();

        var validationResult = validator.Validate(validInput);

        Assert.True(validationResult.IsValid);
    }

    [Theory]
    [InlineData("abc\n123")]
    [InlineData("abc\t123")]
    [InlineData("abc  123")]
    public void DisallowNonSpaceWhitespace_NonspaceWhitespace_Invalid(string nonspaceWhitespace)
    {
        var validator = new FakeValidator();

        var validationResult = validator.Validate(nonspaceWhitespace);

        Assert.False(validationResult.IsValid);
    }

    private class FakeValidator : AbstractValidator<string>
    {
        public FakeValidator()
        {
            RuleFor(s => s).DisallowNonSpaceWhitespace();
        }
    }
}