// HACK: https://www.freecodecamp.org/news/how-to-implement-runtime-environment-variables-with-create-react-app-docker-and-nginx-7f9d42a91d70/
// This file will be replaced via configure-env.sh prior to running the application
window._env_ = {
    API_BASE_URL: 'http://localhost:5001'
}