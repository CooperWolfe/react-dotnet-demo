import './App.css';
import Header from './features/header/Header';
import { Container, ToastContainer } from 'react-bootstrap';
import { Route, Routes } from 'react-router';
import Recipes from './features/recipes/Recipes';
import Footer from './features/footer/Footer';
import Toast from './features/toast/Toast';
import RootRecipe from './features/recipes/Recipe/RootRecipe';

function App() {
  return (
    <Container className="App">
      <ToastContainer position='top-center' className='pt-3'>
        <Toast/>
      </ToastContainer>
      <Header></Header>
      <Routes>
        <Route index element={<Recipes></Recipes>} />
        <Route path="recipe/:id" element={<RootRecipe/>} />
      </Routes>
      <Footer></Footer>
    </Container>
  );
}

export default App;
