import axios from 'axios'
import { showToast } from '../features/toast/toastSlice'
import { API_BASE_URL } from './environment'
import { store } from './store'

export const httpClient = axios.create({
  baseURL: API_BASE_URL,
  responseType: "json",
})
httpClient.interceptors.response.use(
  (response) => {
    if (response.status >= 400) {
      store.dispatch(showToast(response.statusText))
      throw response
    }
    return response
  },
  (error) => {
    if (error.message)
      store.dispatch(showToast(error.message))
    return error
  })
httpClient.defaults.headers.common['Content-Type'] = 'application/json'