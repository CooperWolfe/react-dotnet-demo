import LocalizedStrings from 'react-localization'

export const strings = new LocalizedStrings({
  en: {
    save: "Save",
    cancel: "Cancel",
    delete: "Delete",
    edit: "Edit",
    ingredients: "Ingredients",
    instructions: "Instructions"
  }
})