import React, { FormEvent } from "react";
import { connect } from "react-redux";
import { deleteRecipe, deleteRecipeImage, getRecipe, GetRecipeRequest, GetRecipeResponse, setRecipeImage, SetRecipeImageRequest, updateRecipe, UpdateRecipeRequest } from "../recipesAPI";
import { Recipe as RecipeModel, refreshRecipeImage, removeRecipe, setRecipe } from '../recipesSlice'
import RecipeSummary from "./RecipeSummary";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { API_BASE_URL } from "../../../app/environment";
import style from '../Recipes.module.css'
import RecipeForm from "./RecipeForm";
import { strings } from "../../../app/localization";

const mapProps = (store: {  }) => {
  return {
  }
};
const mapDispatch = { setRecipe, refreshRecipeImage, removeRecipe };

interface Props {
  recipe: RecipeModel
  setRecipe: (payload: { id: string, recipe: RecipeModel }) => void
  refreshRecipeImage: (payload: { id: string }) => void
  removeRecipe: (payload: string) => void
  afterDelete?: () => void
}
type Mode = 'read' | 'write'
interface State {
  until: Date
  mode: Mode
  isImageHovering: boolean,
  status: 'loading' | 'idle'
}

class Recipe extends React.Component<Props, State> {
  imageForm: HTMLFormElement | null = null

  constructor(props: any) {
    super(props)
    this.state = {
      until: new Date(),
      mode: 'read',
      isImageHovering: false,
      status: 'idle'
    }
  }

  async componentDidMount() {
    const response = await this.executeAsync(getRecipe(new GetRecipeRequest(this.props.recipe.id)))
    this.setRecipe(this.props.recipe.id, response)
  }
  private async executeAsync<T>(action: Promise<T>): Promise<T> {
    this.setState(state => ({ ...state, status: 'loading' }))
    try {
      return await action
    } finally {
      this.setState(state => ({ ...state, status: 'idle' }))
    }
  }
  private setRecipe = (id: string, response: GetRecipeResponse) => {
    this.props.setRecipe({
      id: id,
      recipe: {
        ...response,
        id: id,
        imageId: "abc"
      }
    })
  }

  toggleMode = () => {
    this.setState(state => ({ ...state, mode: this.state.mode === 'read' ? 'write' : 'read' }))
  }
  enableImageEdit = () => {
    this.setState(state => ({ ...state, isImageHovering: true }))
  }
  disableImageEdit = () => {
    this.setState(state => ({ ...state, isImageHovering: false }))
  }
  updateImage = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const image = new FormData(this.imageForm!)
    const request = new SetRecipeImageRequest(this.props.recipe.id, image)
    await this.executeAsync(setRecipeImage(request))
    this.props.refreshRecipeImage({ id: this.props.recipe.id })
  }
  handleDelete = async () => {
    const previousRecipe = this.props.recipe
    try {
      this.props.removeRecipe(this.props.recipe.id)
      await this.executeAsync(deleteRecipe(this.props.recipe.id))
      this.props.afterDelete && this.props.afterDelete()
    } catch {
      this.props.setRecipe({ id: previousRecipe.id, recipe: previousRecipe })
    }
  }
  handleDeleteImage = async () => {
    await this.executeAsync(deleteRecipeImage(this.props.recipe.id))
    this.props.refreshRecipeImage({ id: this.props.recipe.id })
  }
  handleSave = async (recipe: RecipeModel) => {
    const previousRecipe = this.props.recipe
    try {
      this.props.setRecipe({
        id: recipe.id,
        recipe: recipe
      })
      await this.executeAsync(updateRecipe(new UpdateRecipeRequest(
        recipe.id,
        recipe.name,
        recipe.description,
        recipe.ingredients!.map(i => ({
          name: i.name,
          amountValue: i.amountValue,
          amountUnit: i.amountUnit
        })),
        recipe.instructions!.map(i => ({
          description: i.description
        })))))
      this.toggleMode()
    } catch {
      this.props.setRecipe({ id: previousRecipe.id, recipe: previousRecipe })
    }
  }

  render(): React.ReactNode {
    return (
      <Container className="Recipe">
        <Row>
          <Col className="col-auto col-md"></Col>
          <Col className={style.recipeDetailImage}>
            <Card onMouseOver={this.enableImageEdit} onMouseOut={this.disableImageEdit} bg="dark" text="white" className="col-auto">
              <Card.Img src={`${API_BASE_URL}/recipe/${this.props.recipe.id}/image?state=${this.props.recipe.imageId}`}></Card.Img>
              {this.state.isImageHovering && <Card.ImgOverlay className={style.recipeDetailImageOverlay}>
                <Form ref={(me: HTMLFormElement) => { this.imageForm = me }} encType="multipart/form-data" onSubmit={(e) => this.updateImage(e)} className="mb-3">
                  <Form.Control onChange={() => this.imageForm!.requestSubmit()} type="file" id="image" name="image" aria-label="image" accept=".jpg,.png" className="btn btn-success" disabled={this.state.status === 'loading'} />
                </Form>
                <Button variant="danger" onClick={this.handleDeleteImage}>{strings.delete}</Button>
              </Card.ImgOverlay>}
            </Card>
          </Col>
          <Col className="col-auto col-md"></Col>
        </Row>
        {this.state.mode === 'read'
          ? <RecipeSummary recipe={this.props.recipe} onEdit={this.toggleMode} isLoading={this.state.status === 'loading'} />
          : <RecipeForm isLoading={this.state.status === 'loading'} recipe={this.props.recipe} onCancel={this.toggleMode} onDelete={this.handleDelete} onSave={this.handleSave} />}
      </Container>
    )
  }
}

export default connect(mapProps, mapDispatch)(Recipe);