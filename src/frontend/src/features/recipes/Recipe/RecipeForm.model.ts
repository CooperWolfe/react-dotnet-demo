import { Ingredient, Instruction, Recipe } from "../recipesSlice"

declare namespace RecipeFormModel {
  type Ingredient = typeof RecipeFormModel.Ingredient.prototype
  type Instruction = typeof RecipeFormModel.Instruction.prototype
}
export class RecipeFormModel {
  constructor(recipe?: Recipe) {
    this.name = recipe?.name || ""
    this.description = recipe?.description || ""
    this.ingredients = recipe?.ingredients!.map(i => new RecipeFormModel.Ingredient(i)) || [new RecipeFormModel.Ingredient()]
    this.instructions = recipe?.instructions!.map(i => new RecipeFormModel.Instruction(i)) || [new RecipeFormModel.Instruction()]
  }
  name: string
  description: string
  ingredients: Array<RecipeFormModel.Ingredient>
  instructions: Array<RecipeFormModel.Instruction>

  static Ingredient = class {
    constructor(ingredient?: Ingredient) {
      this.name = ingredient?.name || ""
      this.amountValue = ingredient?.amountValue || 1
      this.amountUnit = ingredient?.amountUnit
    }
    name: string
    amountValue: number
    amountUnit?: string
  }
  static Instruction = class {
    constructor(instruction?: Instruction) {
      this.description = instruction?.description || ""
    }
    description: string
  }
}