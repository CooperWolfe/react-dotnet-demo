import React from "react";
import { Button, ButtonGroup, ButtonToolbar, CloseButton, Col, Container, Form, InputGroup, ListGroup, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { RecipeFormModel } from "./RecipeForm.model";
import style from '../Recipes.module.css'
import { Recipe, setRecipe } from "../recipesSlice";
import { strings } from "../../../app/localization";

const mapProps = (store: {  }) => {
  return {
  }
};
const mapDispatch = { setRecipe }

interface Props {
  recipe?: Recipe
  onCancel?: () => void
  onDelete?: () => void
  onSave?: (recipe: Recipe) => void
  setRecipe: (payload: { id: string, recipe: Recipe }) => void
  isLoading: boolean
}
interface State {
  recipe: RecipeFormModel
  validated: boolean
}

class RecipeForm extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      recipe: new RecipeFormModel(props.recipe),
      validated: false
    }
  }

  private async executeAsync<T>(action: Promise<T>): Promise<T> {
    this.setState(state => ({ ...state, status: 'loading' }))
    try {
      return await action
    } finally {
      this.setState(state => ({ ...state, status: 'idle' }))
    }
  }

  handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    this.setState(state => ({ ...state, validated: true }))
    if (!(e.target as HTMLFormElement)?.checkValidity()) return
    this.props.onSave && this.props.onSave({
      ...this.state.recipe,
      id: this.props.recipe?.id || "pending",
      imageId: this.props.recipe?.imageId || "abc",
      ingredients: this.state.recipe.ingredients.map(i => ({ ...i })),
      instructions: this.state.recipe.instructions.map(i => ({ ...i }))
    })
  }
  handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newName = e.target.value.replaceAll(/\s{2,}/g, " ").substring(0, 50)
    this.setState(state => ({ recipe: { ...state.recipe, name: newName } }))
  }
  handleDescriptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newDescription = e.target.value.replaceAll(/[^\S ]/g, "").replaceAll(/\s{2,}/g, " ").substring(0, 65535)
    this.setState(state => ({ recipe: { ...state.recipe, description: newDescription } }))
  }
  handleAmountValueChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, i: number) => {
    let newAmountValue = +e.target.value
    if (newAmountValue < 1) newAmountValue = 1
    if (newAmountValue >= 10000) newAmountValue = 9999
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        ingredients: [
          ...state.recipe.ingredients.slice(0, i),
          { ...state.recipe.ingredients[i], amountValue: newAmountValue },
          ...state.recipe.ingredients.slice(i+1)
        ]
      }
    }))
  }
  handleAmountUnitChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, i: number) => {
    const newAmountValue = e.target.value.substring(0, 5).replaceAll(/\s/g, "")
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        ingredients: [
          ...state.recipe.ingredients.slice(0, i),
          { ...state.recipe.ingredients[i], amountUnit: newAmountValue },
          ...state.recipe.ingredients.slice(i+1)
        ]
      }
    }))
  }
  handleIngredientNameChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, i: number) => {
    const newName = e.target.value.replaceAll(/\s{2,}/g, " ").substring(0, 65535)
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        ingredients: [
          ...state.recipe.ingredients.slice(0, i),
          { ...state.recipe.ingredients[i], name: newName },
          ...state.recipe.ingredients.slice(i+1)
        ]
      }
    }))
  }
  handleInstructionDescriptionChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, i: number) => {
    const newDescription = e.target.value.substring(0, 65535)
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        instructions: [
          ...state.recipe.instructions.slice(0, i),
          { ...state.recipe.instructions[i], description: newDescription },
          ...state.recipe.instructions.slice(i+1)
        ]
      }
    }))
  }
  handleDeleteIngredient = (i: number) => {
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        ingredients: [
          ...state.recipe.ingredients.slice(0, i),
          ...state.recipe.ingredients.slice(i+1)
        ]
      }
    }))
  }
  handleDeleteInstruction = (i: number) => {
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        instructions: [
          ...state.recipe.instructions.slice(0, i),
          ...state.recipe.instructions.slice(i+1)
        ]
      }
    }))
  }
  handleAddIngredient = () => {
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        ingredients: [
          ...state.recipe.ingredients,
          { name: "", amountValue: 1, amountUnit: "" }
        ]
      }
    }))
  }
  handleAddInstruction = () => {
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        instructions: [
          ...state.recipe.instructions,
          { description: "" }
        ]
      }
    }))
  }
  handleMoveIngredientUp = (i: number) => {
    if (i === 0) return
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        ingredients: [
          ...state.recipe.ingredients.slice(0, i - 1),
          state.recipe.ingredients[i],
          state.recipe.ingredients[i-1],
          ...state.recipe.ingredients.slice(i + 1),
        ]
      }
    }))
  }
  handleMoveIngredientDown = (i: number) => {
    if (i === this.state.recipe.ingredients.length-1) return
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        ingredients: [
          ...state.recipe.ingredients.slice(0, i),
          state.recipe.ingredients[i+1],
          state.recipe.ingredients[i],
          ...state.recipe.ingredients.slice(i + 2),
        ]
      }
    }))
  }
  handleMoveInstructionUp = (i: number) => {
    if (i === 0) return
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        instructions: [
          ...state.recipe.instructions.slice(0, i - 1),
          state.recipe.instructions[i],
          state.recipe.instructions[i-1],
          ...state.recipe.instructions.slice(i + 1),
        ]
      }
    }))
  }
  handleMoveInstructionDown = (i: number) => {
    if (i === this.state.recipe.instructions.length-1) return
    this.setState(state => ({
      recipe: {
        ...state.recipe,
        instructions: [
          ...state.recipe.instructions.slice(0, i),
          state.recipe.instructions[i+1],
          state.recipe.instructions[i],
          ...state.recipe.instructions.slice(i + 2),
        ]
      }
    }))
  }

  render(): React.ReactNode {
    return (
      <Container className="RecipeForm mt-3">
        <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>
          <Row>
            <Col className="col-auto col-md"></Col>
            <Col>
              <Form.Group className="mb-3" controlId="name">
                <Form.Control required aria-label="Name" placeholder="Name" type="text" min={1} max={9999}
                  value={this.state.recipe.name} onChange={this.handleNameChange} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="description">
                <Form.Control as="textarea" required aria-label="Description" placeholder="Description"
                  value={this.state.recipe.description} onChange={this.handleDescriptionChange} />
              </Form.Group>
            </Col>
            <Col className="col-auto col-md"></Col>
          </Row>
          <hr/>
          <div className={style.recipeDetails}>
            <h3>{strings.ingredients}</h3>
            <ListGroup>
              {this.state.recipe.ingredients.map((ingredient, i) =>
                <ListGroup.Item key={i}>
                  <Row className="mb-2">
                    <Col>
                      <Row>
                        <Col className="col-auto">
                          <Form.Group>
                            <Form.Text id={`amountLabel${i}`}>Amount</Form.Text>
                            <InputGroup aria-describedby={`amountLabel${i}`}>
                              <Form.Control style={{ width: 100 }} aria-label="Amount" type="number"
                                value={ingredient.amountValue} onChange={(e) => this.handleAmountValueChange(e, i)}/>
                              <Form.Control style={{ width: 100 }} aria-label="Amount Unit" placeholder="Unit" type="text"
                                value={ingredient.amountUnit || ""} onChange={(e) => this.handleAmountUnitChange(e, i)}/>
                            </InputGroup>
                          </Form.Group>
                        </Col>
                        <Col className="col-12 col-md">
                          <Form.Group>
                            <Form.Text id={`ingredientName${i}`}>Name</Form.Text>
                            <Form.Control required aria-label={`Ingredient #${i+1} Name`} placeholder={`Ingredient #${i+1} Name`} type="text" aria-describedby={`ingredientName${i}`}
                              value={ingredient.name} onChange={(e) => this.handleIngredientNameChange(e, i)} />
                          </Form.Group>
                        </Col>
                      </Row>
                    </Col>
                    {i > 0 &&
                      <Col className={style.buttonColumn}>
                        <i className={"bi bi-chevron-up " + style.arrowButton} onClick={() => this.handleMoveIngredientUp(i)} />
                      </Col>}
                    {i < this.state.recipe.ingredients.length - 1 &&
                      <Col className={style.buttonColumn}>
                        <i className={"bi bi-chevron-down " + style.arrowButton} onClick={() => this.handleMoveIngredientDown(i)} />
                      </Col>}
                    {this.state.recipe.ingredients.length > 1 &&
                      <Col className={style.buttonColumn}>
                        <CloseButton type="button" onClick={() => this.handleDeleteIngredient(i)} />
                      </Col>}
                  </Row>
                </ListGroup.Item>
              )}
              <ListGroup.Item>
                <Button variant="success" type="button" onClick={() => this.handleAddIngredient()}>+</Button>
              </ListGroup.Item>
              <hr />
              <h3>{strings.instructions}</h3>
              <ListGroup>
                {this.state.recipe.instructions.map((instruction, i) =>
                  <ListGroup.Item key={i}>
                    <Form.Group>
                      <Form.Text id={`instruction${i}`}>Description</Form.Text>
                      <InputGroup aria-describedby={`instruction${i}`}>
                        <InputGroup.Text id={`instructionNum${i}`}>{i+1}.</InputGroup.Text>
                        <Form.Control required as="textarea" aria-describedby={`instructionNum${i}`} aria-label={`Instruction #${i + 1} Description`} placeholder={`Instruction #${i + 1} Description`}
                          value={instruction.description} onChange={(e) => this.handleInstructionDescriptionChange(e, i)} />
                        {i > 0 &&
                          <InputGroup.Text>
                            <i className={"bi bi-chevron-up " + style.arrowButton} onClick={() => this.handleMoveInstructionUp(i)} />
                          </InputGroup.Text>}
                        {i < this.state.recipe.instructions.length - 1 &&
                          <InputGroup.Text>
                            <i className={"bi bi-chevron-down " + style.arrowButton} onClick={() => this.handleMoveInstructionDown(i)} />
                          </InputGroup.Text>}
                        {this.state.recipe.instructions.length > 1 &&
                          <InputGroup.Text>
                            <CloseButton type="button" onClick={() => this.handleDeleteInstruction(i)} />
                          </InputGroup.Text>}
                      </InputGroup>
                    </Form.Group>
                  </ListGroup.Item>
                )}
                <ListGroup.Item>
                  <Button variant="success" type="button" onClick={() => this.handleAddInstruction()}>+</Button>
                </ListGroup.Item>
              </ListGroup>
            </ListGroup>
            <ButtonToolbar className="mt-3 justify-content-between">
              <ButtonGroup>
                <Button variant="success" type="submit" disabled={this.props.isLoading}>{strings.save}</Button>
                {this.props.isLoading || <Button variant="dark" type="button" onClick={this.props.onCancel}>{strings.cancel}</Button>}
              </ButtonGroup>
              {this.props.recipe &&
                <ButtonGroup>
                  <Button variant="danger" type="button" disabled={this.props.isLoading}
                    onClick={() => this.props.onDelete && this.props.onDelete()}>{strings.delete}</Button>
                </ButtonGroup>}
            </ButtonToolbar>
          </div>
        </Form>
      </Container>
    )
  }
}

export default connect(mapProps, mapDispatch)(RecipeForm);