import React from "react";
import { connect } from "react-redux";
import { Button, ButtonGroup, ListGroup, Spinner } from "react-bootstrap";
import style from '../Recipes.module.css'
import { Recipe } from "../recipesSlice";
import { strings } from "../../../app/localization";

const mapProps = (store: {  }) => {
  return {
  }
};
const mapDispatch = {  };

interface Props {
  recipe: Recipe
  onEdit?: () => void
  isLoading: boolean
}
interface State {
}

class RecipeSummary extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      status: 'idle'
    }
  }

  componentDidMount() {
  }

  render(): React.ReactNode {
    return (
      <div className="RecipeSummary">
        <h2>{this.props.recipe.name}</h2>
        <p>{this.props.recipe.description}</p>
        <hr />
        <div className={style.recipeDetails}>
          {this.props.isLoading && !this.props.recipe.ingredients
            ? <Spinner />
            : <React.Fragment>
              <h3>{strings.ingredients}</h3>
              <ListGroup>
                {this.props.recipe.ingredients?.map((ingredient, i) => (
                  <ListGroup.Item key={i}>{ingredient.amountValue + (ingredient.amountUnit ? ` ${ingredient.amountUnit}` : "")} - {ingredient.name}</ListGroup.Item>))}
              </ListGroup>
              <hr />
              <h3>{strings.instructions}</h3>
              <ListGroup as="ol" numbered>
                {this.props.recipe.instructions?.map((instruction, i) => (
                  <ListGroup.Item as="li" key={i}>{instruction.description}</ListGroup.Item>))}
              </ListGroup>
              <ButtonGroup className="mt-3">
                <Button variant="success" type="button" onClick={() => this.props.onEdit && this.props.onEdit()} disabled={this.props.isLoading}>{strings.edit}</Button>
              </ButtonGroup>
            </React.Fragment>}
        </div>
      </div>
    )
  }
}

export default connect(mapProps, mapDispatch)(RecipeSummary);