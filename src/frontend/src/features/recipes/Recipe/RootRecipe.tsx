import React from "react";
import { getRecipe, GetRecipeRequest } from "../recipesAPI";
import Recipe from "./Recipe";
import { Recipe as RecipeModel, setRecipe } from '../recipesSlice'
import { connect } from "react-redux";
import { RootState } from "../../../app/store";
import styles from '../Recipes.module.css'
import { NavLink } from "react-router-dom";

function getRecipeId(): string {
  return window.location.pathname.replace(/\/$/, '').substring(window.location.pathname.lastIndexOf("/")+1)
}

const mapProps = (store: RootState) => {
  return {
    recipe: store.recipes.recipes && store.recipes.recipes[getRecipeId()]
  }
};
const mapDispatch = { setRecipe };

interface Props {
  recipe?: RecipeModel
  setRecipe: (payload: { id: string, recipe: RecipeModel }) => void
}
interface State {
  isLoading: boolean
}

class RootRecipe extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      isLoading: false,
    }
  }

  async componentDidMount() {
    this.setState(state => ({ ...state, isLoading: true }))
    try {
      const recipeId = getRecipeId()
      const response = await getRecipe(new GetRecipeRequest(recipeId))
      this.props.setRecipe({
        id: recipeId,
        recipe: {
          ...response,
          id: recipeId,
          imageId: "abc"
        }
      })
    } finally {
      this.setState(state => ({ ...state, isLoading: false }))
    }
  }

  render(): React.ReactNode {
    return (
      <div className="RootRecipe">
        <div className={styles.backButton + " mb-3"}>
          <NavLink to="/" className="btn btn-secondary"><i className="bi bi-chevron-left"/> Back</NavLink>
        </div>
        {this.props.recipe && <Recipe recipe={this.props.recipe} afterDelete={() => window.location.assign("/")} />}
      </div>
    )
  }
}

export default connect(mapProps, mapDispatch)(RootRecipe)