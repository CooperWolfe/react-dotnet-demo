import React from "react";
import { Accordion, Button, Col, Form, Image, InputGroup, Row, Spinner } from "react-bootstrap";
import { AccordionEventKey } from "react-bootstrap/esm/AccordionContext";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { API_BASE_URL } from "../../app/environment";
import Recipe from "./Recipe/Recipe";
import RecipeForm from "./Recipe/RecipeForm";
import styles from './Recipes.module.css'
import { createRecipe, CreateRecipeRequest, searchRecipes, SearchRecipesRequest, SearchRecipesResponse } from "./recipesAPI";
import { RecipesDictionary, RecipesState, setRecipes, Recipe as RecipeModel, setRecipe } from "./recipesSlice";

const mapProps = (store: { recipes: RecipesState }) => {
  return {
    recipes: store.recipes.recipes
  }
};
const mapDispatch = { setRecipes, setRecipe };

interface Props {
  recipes: RecipesDictionary | undefined
  setRecipes: (payload: RecipesDictionary) => void
  setRecipe: (payload: { id: string, recipe: RecipeModel }) => void
}
interface State {
  until: Date
  openRecipeId: string | null
  status: 'loading' | 'idle' | 'creating'
  isCreating: boolean
}

class Recipes extends React.Component<Props, State> {
  searchInput: HTMLInputElement | null = null

  constructor(props: any) {
    super(props)
    this.state = {
      until: new Date(),
      openRecipeId: null,
      status: 'idle',
      isCreating: false
    }
  }

  async componentDidMount() {
    const response = await this.executeAsync(searchRecipes(new SearchRecipesRequest(10, this.state.until)))
    this.setRecipes(response)
  }
  private async executeAsync<T>(action: Promise<T>): Promise<T> {
    this.setState(state => ({ ...state, status: 'loading' }))
    try {
      return await action
    } finally {
      this.setState(state => ({ ...state, status: 'idle' }))
    }
  }
  private setRecipes = (response: SearchRecipesResponse) => {
    this.props.setRecipes(response.recipes.reduce((acc, cur) => ({
      ...acc,
      [cur.id]: {
        ...((this.props.recipes && this.props.recipes[cur.id]) || {}),
        ...cur
      }
    }), {}))
  }

  search = async (e: React.FormEvent) => {
    e.preventDefault();
    const until = new Date()
    const response = await this.executeAsync(searchRecipes(new SearchRecipesRequest(10, until, 0, this.searchInput!.value)))
    this.setRecipes(response)
    this.setState(state => ({ ...state, until }))
  }
  trackOpen = (recipeId: AccordionEventKey) => {
    this.setState(state => {
      if (recipeId instanceof Array<string>) recipeId = recipeId[0]
      if (recipeId === undefined) recipeId = null
      return { ...state, openRecipeId: recipeId }
    })
  }
  toggleCreating = () => {
    this.setState(state => ({ ...state, isCreating: !state.isCreating }))
  }
  handleSave = async (recipe: RecipeModel) => {
    this.setState(state => ({ ...state, status: "creating" }))
    try {
      const response = await createRecipe(new CreateRecipeRequest(
        recipe.name,
        recipe.description,
        recipe.ingredients!,
        recipe.instructions!))
      recipe.id = response.createdRecipeId
      this.props.setRecipe({ id: recipe.id, recipe: recipe })
      this.toggleCreating()
      this.setState(state => ({ ...state, openRecipeId: response.createdRecipeId }))
    } finally {
      this.setState(state => ({ ...state, status: "idle" }))
    }
  }

  render(): React.ReactNode {
    return (
      <div className="Recipes">
        <Form onSubmit={e => this.search(e)}>
          <InputGroup className={styles.inputGroup}>
            <Form.Control ref={(me: HTMLElement | null) => { this.searchInput = me as HTMLInputElement }}
              className={styles.searchInput} placeholder="Search" aria-label="Search" aria-describedby="searchButton"></Form.Control>
            <Button type="submit" disabled={this.state.status !== 'idle'} variant="outline-secondary" id="searchButton">Search</Button>
          </InputGroup>
        </Form>
          {this.props.recipes && Object.keys(this.props.recipes).length > 0
            ? <Accordion onSelect={recipeId => this.trackOpen(recipeId)} className="mt-2 mb-3">
              {Object.keys(this.props.recipes).map(recipeId => (
                <Accordion.Item key={recipeId} eventKey={recipeId}>
                  <Accordion.Header>
                    <Row className="container">
                      <Col md={3}>
                        <Image thumbnail src={`${API_BASE_URL}/recipe/${recipeId}/image?state=${this.props.recipes![recipeId].imageId}`} width={128}></Image>
                      </Col>
                      <Col md={3}><h5>{this.props.recipes![recipeId].name} <NavLink to={`/recipe/${recipeId}`} className="bi bi-box-arrow-up-right" /></h5></Col>
                      <Col md={6}>{this.props.recipes![recipeId].description}</Col>
                    </Row>
                  </Accordion.Header>
                  <Accordion.Body>
                    {this.state.openRecipeId === recipeId &&
                      <Recipe recipe={this.props.recipes![recipeId]} />}
                  </Accordion.Body>
                </Accordion.Item>
              ))}
            </Accordion>
            : this.state.status === 'loading'
              ? <Spinner/>
              : <span>There are no recipes.</span>}
          {this.state.isCreating ||
            <div className={styles.newButton}>
              <Button variant="success" type="button" onClick={this.toggleCreating}>New</Button>
            </div>}
          {this.state.isCreating &&
            <RecipeForm isLoading={this.state.status === 'creating'} onCancel={this.toggleCreating} onSave={this.handleSave} />}
      </div>
    )
  }
}

export default connect(mapProps, mapDispatch)(Recipes);