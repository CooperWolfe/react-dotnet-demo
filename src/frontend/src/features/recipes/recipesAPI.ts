import { AxiosResponse } from "axios";
import { httpClient } from "../../app/httpClient";

const sleepTime = 0
function sleep(ms: number): Promise<void> {
  return new Promise<void>((resolve) => setTimeout(resolve, ms))
}

export class SearchRecipesRequest {
  constructor(
    public limit: number,
    public until: Date,
    public offset: number = 0,
    public query?: string) {}
}
export interface SearchRecipesResponse {
  recipes: [
    {
      id: string,
      name: string,
      description: string
    }
  ]
}
export async function searchRecipes(request: SearchRecipesRequest) {
  await sleep(sleepTime);
  const response = await httpClient.get<any, AxiosResponse<SearchRecipesResponse, any>, any>("recipe", {
    params: {
      query: request.query,
      offset: request.offset,
      limit: request.limit,
      until: request.until.toISOString()
    }
  })
  return response.data
}

export class GetRecipeRequest {
  constructor(
    public id: string) { }
}
export interface GetRecipeResponse {
  name: string
  description: string
  ingredients: Array<{
    name: string
    amountValue: number
    amountUnit?: string
  }>
  instructions: Array<{
    description: string
  }>
}
export async function getRecipe(request: GetRecipeRequest) {
  await sleep(sleepTime)
  const response = await httpClient.get<any, AxiosResponse<GetRecipeResponse, any>, any>(`recipe/${request.id}`)
  return response.data
}

export class SetRecipeImageRequest {
  constructor(
    public id: string,
    public data: FormData) { }
}
export async function setRecipeImage(request: SetRecipeImageRequest) {
  await sleep(sleepTime)
  await httpClient.post(`recipe/${request.id}/image`, request.data)
}

export class UpdateRecipeRequest {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public ingredients: Array<{
      name: string,
      amountValue: number,
      amountUnit?: string
    }>,
    public instructions: Array<{
      description: string
    }>) { }
}
export async function updateRecipe(request: UpdateRecipeRequest) {
  await sleep(sleepTime)
  let body: any = { ...request }
  delete body.id
  await httpClient.put(`recipe/${request.id}`, body)
}

export async function deleteRecipe(recipeId: string) {
  await sleep(sleepTime)
  await httpClient.delete(`recipe/${recipeId}`)
}

export class CreateRecipeRequest {
  constructor(
    public name: string,
    public description: string,
    public ingredients: Array<{
      name: string,
      amountValue: number,
      amountUnit?: string
    }>,
    public instructions: Array<{
      description: string
    }>) { }
}
export interface CreateRecipeResponse {
  createdRecipeId: string
}
export async function createRecipe(request: CreateRecipeRequest) {
  await sleep(sleepTime)
  const response = await httpClient.post<any, AxiosResponse<CreateRecipeResponse, any>, any>("recipe", request)
  return response.data
}

export async function deleteRecipeImage(id: string) {
  await sleep(sleepTime)
  await httpClient.delete(`recipe/${id}/image`)
}