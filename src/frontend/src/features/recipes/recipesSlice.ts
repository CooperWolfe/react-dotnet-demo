import { createSlice } from '@reduxjs/toolkit';
import uuid from 'react-uuid'

export interface Recipe {
  id: string
  name: string
  description: string
  ingredients?: Ingredient[]
  instructions?: Instruction[]
  imageId: string
  previousRecipe?: Recipe
}
export interface Ingredient {
  name: string
  amountValue: number
  amountUnit?: string
}
export interface Instruction {
  description: string
}

export type RecipesDictionary = { [id: string]: Recipe }
export interface RecipesState {
  recipes?: RecipesDictionary
}
const initialState: RecipesState = {
};

export const recipesSlice = createSlice({
  name: 'recipes',
  initialState,
  reducers: {
    setRecipes: (state, action: { payload: RecipesDictionary }) => {
      state.recipes = action.payload
    },
    setRecipe: (state, action: { payload: { id: string, recipe: Recipe } }) => {
      if (!state.recipes) state.recipes = {}
      state.recipes[action.payload.id] = action.payload.recipe
    },
    refreshRecipeImage: (state, action: { payload: { id: string } }) => {
      state.recipes![action.payload.id].imageId = uuid()
    },
    removeRecipe: (state, action: { payload: string }) => {
      delete state.recipes![action.payload]
    }
  }
});

export const { setRecipes, setRecipe, refreshRecipeImage, removeRecipe } = recipesSlice.actions;

export default recipesSlice.reducer;
