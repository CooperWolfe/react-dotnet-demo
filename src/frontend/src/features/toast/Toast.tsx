import React from "react";
import { connect } from "react-redux";
import { Toast as BToast } from 'react-bootstrap'
import { hideToast, ToastState } from "./toastSlice";

const mapProps = (store: { toast: ToastState }) => {
  return {
    showToast: store.toast.showToast,
    message: store.toast.message
  }
};
const mapDispatch = { hideToast };

interface Props {
  showToast: boolean
  message: string
  hideToast: (_: any) => any
}
interface State {
}

class Toast extends React.Component<Props, State> {
  render(): React.ReactNode {
    return (
      <div className="Toast">
        <BToast onClose={() => this.props.hideToast(null)} autohide show={this.props.showToast} delay={2400} bg="danger">
          <BToast.Header><strong className="me-auto">An error occurred!</strong></BToast.Header>
          {this.props.message && <BToast.Body>{this.props.message}</BToast.Body>}
        </BToast>
      </div>
    )
  }
}

export default connect(mapProps, mapDispatch)(Toast);