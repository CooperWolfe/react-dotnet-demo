import { createSlice } from '@reduxjs/toolkit';

export interface ToastState {
  showToast: boolean
  message: string
}
const initialState: ToastState = {
  showToast: false,
  message: "An error occurred."
}

export const toastSlice = createSlice({
  name: 'toast',
  initialState,
  reducers: {
    showToast: (state, action) => {
      state.showToast = true
      state.message = action.payload
    },
    hideToast: (state, action) => {
      state.showToast = false
    }
  }
});

export const { showToast, hideToast } = toastSlice.actions

export default toastSlice.reducer;
